<?php
/*
Template Name: Contact
*/
// LEAFLET
/*
function load_leaflet_lib() {
  wp_enqueue_style( 'leafletcss', 'https://unpkg.com/leaflet@1.7.1/dist/leaflet.css', false );
  wp_register_script('leafletjs', 'https://unpkg.com/leaflet@1.7.1/dist/leaflet.js', '', '', true);
  wp_enqueue_script('leafletjs');
}
add_action( 'wp_enqueue_scripts', 'load_leaflet_lib' );
*/

get_clean_header();
if (have_posts()) : while (have_posts()) : the_post();
?>




<main class="contactpage">
  <div class="mainwrapper">

    <div class="contactpage__content">
      <h1><?php the_title(); ?></h1>
      <?php 
      echo wpautop($post->post_content); 
/*
      $blocks = get_field('blocs');
      if (!empty($blocks)) {
        foreach ($blocks as $block) {
          echo BlockBuilder::doBlock($block);
        }
      }
*/      
      ?>
    </div>

    <div class="contactpage__map">
      <h2>Plan</h2>
      <div class="plan">
        <div class="plan__inner" id="leafmap"></div>
      </div>
    </div>

  </div>
</main>




<?php
endwhile; endif;
get_clean_footer();

