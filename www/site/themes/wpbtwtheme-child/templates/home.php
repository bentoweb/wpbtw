<?php
/*
Template Name: Homepage
*/

get_clean_header();
if (have_posts()) : while (have_posts()) : the_post();

  // $img = getImageObj($post->ID,764,0,60,false,true,'avif');
  // $img2 = getImageObj($post->ID,764,0,60,false,true,'notavif');

  $image = getImageTag($post->ID,764,0,50,false,false);
  $image2 = getImageTag($post->ID,764,0,80,false,false);

?>




<main class="homepage">
  <div class="mainwrapper">

    <div class="homepage__content">
      <h1><?php echo $post->post_title; ?></h1>
      <?php 
      echo wpautop($post->post_content); 

      pr('============= IMG =============');

      if (!empty($image)) {
        echo $image;
      }

      if (!empty($image2)) {
        echo $image2;
      }

      if (!empty($img)) {
        echo '<img src="'.$img->src.'" alt="'.$img->post_title.'">';
      }

      if (!empty($img2)) {
        echo '<img src="'.$img2->src.'" alt="'.$img2->post_title.'">';
      }



      $blocks = get_field('blocs');
      if (!empty($blocks)) {
        foreach ($blocks as $block) {
          echo BlockBuilder::doBlock($block);
        }
      }
      ?>
    </div>

  </div>
</main>






<?php

endwhile; endif;
get_clean_footer();

