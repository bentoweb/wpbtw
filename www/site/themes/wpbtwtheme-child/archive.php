<?php
get_clean_header();
$term = get_queried_object();
?>

<div class="archive">

  <?php
  if (have_posts()) : while (have_posts()) : the_post();
    $img = getImageObj($post->ID,500,0,60,false,true);
    $url = get_permalink($post->ID);
  endwhile; endif;
  ?>

  <div class="pagination">
    <?php 
    $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
    $nbr_pages = $wp_query->max_num_pages;
    // $nbr_posts = $term->count;
    echo btw_pagination($paged,$nbr_pages,true,true,get_term_link($term));
    ?>
  </div>

</div>

<?php
get_clean_footer();