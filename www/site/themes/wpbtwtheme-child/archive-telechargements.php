<?php
/*
Template Name: Téléchargements
*/
if (!is_archive()) {
  wp_redirect( get_post_type_archive_link('telechargements'), 308 );
  exit;
}



get_clean_header();
// $term = get_queried_object();

if (is_archive()) {
  // echo 'A';
  // $term = get_queried_object();
  $page = get_post(44);
  $p = $page;
  // echo getPageHeader($page);

} else if (is_page()) {
  // echo 'B';
  $p = $post;
  // echo getPageHeader($post);

} else {
  // echo 'C';

}

// pr($post);

// $obj = get_queried_object();
// pr($term);

// $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
// $ppp = get_option( 'posts_per_page' );
// $offset = ($paged * $ppp) - $ppp;

?>

<div class="archive m--documentation">

  <?php if (!empty($p->post_content)) { ?>
    <div class="mainwrapper">
      <div class="mce-content-body pagebloc__intro">
        <?php echo wpautop($p->post_content) ?>
      </div>
    </div>
  <?php } ?>

  <div class="mainwrapper">

    <div class="filtres">
      <span>Filtrer :</span>
      <?php 
      $terms = get_terms( 'type', array(
        'hide_empty' => false,
      ) );
      // pr($terms);
      if (!empty($terms)) {
        foreach ($terms as $term) {
          $tclass = "";
          if (!empty($obj->term_id) && $obj->term_id==$term->term_id) {
            $tclass = "is--current";
          }
          echo '<a class="'.$tclass.'" href="'.get_term_link($term).'" title="'.$term->name.'"><span>'.$term->name.'</span></a>';
        }
      }
      ?>
    </div>

    <div class="fichiers__items">
    <?php
    if (have_posts()) : while (have_posts()) : the_post();
      // $img = getImageObj($post->ID,500,0,60,false,true);
      $url = get_permalink($post->ID);
      $terms = wp_get_post_terms($post->ID,'type');
      // $fichier = get_field('fichier');
      $fichier = get_field('fichier_prive');
      if (!empty($fichier) && !empty($terms)) {
      ?>
      <div class="fichiers__item <?php if(!empty($terms)){ foreach($terms as $term) { echo ' m--'.$term->slug; }} ?>">
        <div class="fichiers__block">
          <div class="fichiers__content" data-href="<?php echo $url ?>">
            <div class="fichiers__cov" style="<?php 
              $couleur = get_field('couleur',$terms[0]);
              if (!empty($couleur)) {
                echo 'background-color: '.$couleur.';';
              }
            ?>">
              <?php echo svg_icon('icon-file') ?>
              <?php echo svg_icon('icon-bluedownload') ?>
              <div class="cat"><span><?php echo $terms[0]->name ?></span></div>
            </div>
            <div class="fichiers__name">
              <h2><a href="<?php echo $url ?>"><?php echo $post->post_title ?></a></h2>
              <p><?php echo round(intval($fichier['filesize']) / 1024) ?>ko</p>
            </div>
          </div>
        </div>
      </div>
      <?php 
      }
      // pr($url);
    endwhile; endif;
    ?>
    </div>
  </div>

  <div class="pagination">
    <?php 
    // if (is_page()) {
    //   $maxpage = ceil(wp_count_posts()->publish / $ppp);
    //   $baseurl = get_permalink($post->ID);

    // } else if (is_archive()) {
    //   $term = get_queried_object();
    //   $maxpage = $wp_query->max_num_pages;
    //   $baseurl = get_term_link($term);

    // }
    // $lang = array(
    //   'first' => "Première",
    //   'last' => "Dernière",
    //   'previous' => "Précédente",
    //   'next' => "Suivante",
    // );
    // echo btw_pagination($paged,$maxpage,true,true,$baseurl,$lang);
    ?>
  </div>

</div>

<?php
get_clean_footer();