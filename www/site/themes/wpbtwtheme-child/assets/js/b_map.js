(function($) {

  var btwApp = window.btwApp;
  // declare variables here
  var rmap;


  btwApp.readyAndLoaded(function(){      

    if ($('#leafmap').length) {

      var coord = [48.117551779208696, -1.6518628726319513];

      rmap = L.map('leafmap', {
        // center: [46.9119546,2.7503483],
        center: coord,
        zoom: 16
      });

      // L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      L.tileLayer('https://cartodb-basemaps-{s}.global.ssl.fastly.net/light_all/{z}/{x}/{y}.png', {
          attribution: 'Map tiles by Carto, under CC BY 3.0. Data by OpenStreetMap, under ODbL.'
      }).addTo(rmap);

      var marker = L.icon({
        iconUrl: home_url+'/assets/img/map-stick.png',
        // shadowUrl: 'leaf-shadow.png',

        iconSize:     [80, 80], // size of the icon
        // shadowSize:   [50, 64], // size of the shadow
        iconAnchor:   [40, 40], // point of the icon which will correspond to marker's location
        // shadowAnchor: [4, 62],  // the same for the shadow
        // popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
      });

      L.marker(coord, {icon: marker}).addTo(rmap);

    }

  });


  // btwApp.responsive(function(){
    // console.log("do responsive !");
  // });


})(jQuery);