(function($) {

  var btwApp = window.btwApp;
  // declare variables here
  var docReady = false;
  var docLoad = false;

  var menulock = false;
  var menuopen = false;

  // var sections;


  function closeMenu() {
    $('body').removeClass('is--burgermenu_open');
    menuopen = false;
  }


  function openMenu() {
    $('body').addClass('is--burgermenu_open');
    menuopen = true;
  }


  function readyAndLoaded() {
    if (!docLoad || !docReady) { return; }

    // Scripts here

    $('.burgermenu').on('click',function(e){
      console.log('test');
      e.preventDefault();
      if ($('body').hasClass('is--burgermenu_open')) {
        closeMenu();
      } else {
        openMenu();
      }
    }).on('mouseenter',function(e){
      menulock = true;
    }).on('mouseleave',function(e){
      menulock = false;
    });

    $('.mainmenu').on('mouseenter',function(e){
      menulock = true;
    }).on('mouseleave',function(e){
      menulock = false;
    });

    $(document).on('click',function(e){
      if (!menulock && menuopen) {
        closeMenu();
      }
    });

/*
    sections = $('section.frame');
    var currentsection = null;
    var cid = null;
    $(window).on('scroll',function(e) {
      var scrollTop = $(window).scrollTop();

      sections.each(function(e){
        var limit = $(this).offset().top - scrollTop - (btwApp.wH/2);
        if (limit<0) {
          cid = $(this).attr('id');
        }
      });

      if (currentsection!=cid) {
        currentsection = cid;
        $('.mainmenu a').removeClass('is--current');
        $('.mainmenu a[href="#'+currentsection+'"]').addClass('is--current');
      }

    });
*/

/*
    if (ajax_lock) { return false; }
    ajax_lock = true;

    var c_id = $(this).find('.maville').val();

    var data = {
      action : 'getcollectivite',
      c_id : c_id
    };

    $.ajax({
      url: ajax_var.url,
      type: 'POST',
      data: data,
      success : function(msg, statut){
        ajax_lock = false;
        // console.log('ajax:',msg);

        try {
          var r = JSON.parse(msg);
          console.log('r:',r);

        } catch (error) {
          console.error(error);
          // expected output: ReferenceError: nonExistentFunction is not defined
          // Note - error messages will vary depending on browser
        }

      },
      error : function(resultat, statut, erreur){
        ajax_lock = false;
        alert("Erreur de requête");
        // $('.loadmsg').remove();
      }
    });
*/

  }


  // btwApp.responsive(function(){
    // console.log("do responsive !");
  // });


  $(window).on("load", function() {
    docLoad = true;
    readyAndLoaded();
  });


  $(document).ready(function(){
    docReady = true;
    readyAndLoaded();
  });


  // $(window).on('scroll',function(e) {

    // scrolled = true;
    // if (docLoad && docReady) {
    //   anyFunction();
    // }

    // clearTimeout(scrollTimeout);
    // scrollTimeout = setTimeout(function(){
    //   scrollStopped();
    // }, 100);

  // });

})(jQuery);