(function($) {

  var btwApp = window.btwApp;
  // declare variables here


  btwApp.readyAndLoaded(function(){

    $('.faq__question').find('.faq__reponse').slideUp();
    $('.faq__question').find('.faq__openbutton').on('click',function(e){
      e.preventDefault();
      if ($(this).parents('.faq__question').hasClass('is--open')) {
        $(this).parents('.faq__question').removeClass('is--open').find('.faq__reponse').stop().slideUp();
      } else {
        $('.faq__question').removeClass('is--open').find('.faq__reponse').stop().slideUp();
        $(this).parents('.faq__question').addClass('is--open').find('.faq__reponse').stop().slideDown();
      }
    });

  });


  // btwApp.responsive(function(){
    // console.log("do responsive !");
  // });


})(jQuery);