
/*
888     888
888     888
888     888
Y88b   d88P  8888b.  888d888 .d8888b
 Y88b d88P      "88b 888P"   88K
  Y88o88P   .d888888 888     "Y8888b.
   Y888P    888  888 888          X88
    Y8P     "Y888888 888      88888P'
*/
var popin_lock = false;
var lockmenu = false;







/*
8888888888
888
888
8888888    888  888 88888b.   .d8888b
888        888  888 888 "88b d88P"
888        888  888 888  888 888
888        Y88b 888 888  888 Y88b.
888         "Y88888 888  888  "Y8888P
*/
function popin_open() {
  $('div#popin').removeClass('invisible').addClass('visible');
}
function popin_close() {
  $('div#popin').removeClass('visible').addClass('invisible');
}







/*
8888888          d8b 888
  888            Y8P 888
  888                888
  888   88888b.  888 888888
  888   888 "88b 888 888
  888   888  888 888 888
  888   888  888 888 Y88b.
8888888 888  888 888  "Y888
*/
// safari_nobow = true;// Evite l'effet élatisque en haut et en bas d'une page Safari iOS
// if (isIOS()) {}// Détecte iOS
// if (isTouchDevice()) {}// Détecte un device tactile






/*
8888888b.
888   Y88b
888    888
888   d88P  .d88b.  .d8888b  88888b.
8888888P"  d8P  Y8b 88K      888 "88b
888 T88b   88888888 "Y8888b. 888  888
888  T88b  Y8b.          X88 888 d88P
888   T88b  "Y8888   88888P' 88888P"
                             888
                             888
                             888
*/
function responsive() {

  $('.fond_size').each(function(){
    fond_size($(this));
  });

}







/*
8888888b.                         888
888   Y88b                        888
888    888                        888
888   d88P  .d88b.   8888b.   .d88888 888  888
8888888P"  d8P  Y8b     "88b d88" 888 888  888
888 T88b   88888888 .d888888 888  888 888  888
888  T88b  Y8b.     888  888 Y88b 888 Y88b 888
888   T88b  "Y8888  "Y888888  "Y88888  "Y88888
                                           888
                                      Y8b d88P
                                       "Y88P"
*/
$(document).ready(function(){

    //--- Afficher le masque de chargement
    // $('body').append('<div id="loadmask"></div>');

    $('div#popin .popin_frame').on('mouseenter',function(){
      popin_lock = true;
    }).on('mouseleave',function(){
      popin_lock = false;
    });
    $('div#popin').on('click',function(){
      if (popin_lock==false) {
        popin_close();
      }
    });

    $('div.menu_burger').on('click',function(){
      if ($('body').hasClass('openmenu')) {
        $('body').removeClass('openmenu').addClass('closemenu');
      } else {
        $('body').removeClass('closemenu').addClass('openmenu');
      }
    });
    $('div.menu_burger_links').on('mouseenter',function(){
      lockmenu = true;
    });
    $('div.menu_burger_links').on('mouseleave',function(){
      lockmenu = false;
    });
    $(document).on('click',function(){
      if (lockmenu==false) {
        $('body').removeClass('openmenu').addClass('closemenu');
      }
    });

});





/*
888                             888
888                             888
888                             888
888       .d88b.   8888b.   .d88888
888      d88""88b     "88b d88" 888
888      888  888 .d888888 888  888
888      Y88..88P 888  888 Y88b 888
88888888  "Y88P"  "Y888888  "Y88888
*/
$(window).on('load',function(){ setTimeout(function(){


    //--- Cacher et supprimer le masque de chargement
    /*
    $('div#loadmask').addClass('hide');
    setTimeout( function(){ $('div#loadmask').remove(); }, 1000 );
    */

    //--- AJAX
/*
    var actionexempledata = {
      action : 'actionexemple',
      diversdata : 'valeur'
    };
    $.ajax({
        url: ajax_var.url,
        type: 'POST',
        data: actionexempledata
    }).done( function( msg ) {
        // console.log(ajax_var.url+' : '+msg);
        console.log('__'+msg);
    }).error( function(msg) {
        // console.log(ajax_var.url+' : '+msg);
        alert("Erreur de requête");
    });
*/

}, 100); });

















/*
$(function($) {
  $('body').bind('mousewheel', function(event, delta) {
    dir = delta > 0 ? 'Up' : 'Down';
    vel = Math.abs(delta);
    if(dir=='Down'){
      // nextPage();
    } else {
      // prevPage();
    }
    //return false;
  });
});
*/




//--------- keybind


$(document).keydown(function(e){
      var toucheCode = e.keyCode || e.which;
      // $('#footer .testspan').remove();
      // $('#footer p').append('<span class="testspan">'+toucheCode+'</span>');
      // ----------------- flèche haut
      if (toucheCode==38) {
        // prevPage();
      }
      // ----------------- flèche bas
      if (toucheCode==40) {
        // nextPage();
      }
      // ----------------- F
      if (toucheCode==70) {
        // nextPage();
        // RS_nextposts();
      }
      // ----------------- T
      if (toucheCode==84) {
        // nextPage();
        // RS_prevposts();
      }
});

/*

      var nbr_pages_twitter = 0;
      var nbr_pages_facebook = 0;
      var active_page_twitter = 1;
      var active_page_facebook = 1;

*/




/*
    CUBIC-BEZIER
    $("#box").click(function() {
        $(this).animate({
            "margin-left": 200
        }, 2000, $.bez([0.685, 0.595, 0.020, 0.720]));
    });
*/
jQuery.extend({bez:function(encodedFuncName,coOrdArray){if(jQuery.isArray(encodedFuncName)){coOrdArray=encodedFuncName;encodedFuncName="bez_"+coOrdArray.join("_").replace(/\./g,"p")}if(typeof jQuery.easing[encodedFuncName]!=="function"){var polyBez=function(p1,p2){var A=[null,null],B=[null,null],C=[null,null],bezCoOrd=function(t,ax){C[ax]=3*p1[ax],B[ax]=3*(p2[ax]-p1[ax])-C[ax],A[ax]=1-C[ax]-B[ax];return t*(C[ax]+t*(B[ax]+t*A[ax]))},xDeriv=function(t){return C[0]+t*(2*B[0]+3*A[0]*t)},xForT=function(t){var x=t,i=0,z;while(++i<14){z=bezCoOrd(x,0)-t;if(Math.abs(z)<.001)break;x-=z/xDeriv(x)}return x};return function(t){return bezCoOrd(xForT(t),1)}};jQuery.easing[encodedFuncName]=function(x,t,b,c,d){return c*polyBez([coOrdArray[0],coOrdArray[1]],[coOrdArray[2],coOrdArray[3]])(t/d)+b}}return encodedFuncName}});;
