<?php

$t = get_the_title();
$sitename = get_bloginfo('name');
if(is_front_page()) {
  $pageid = 'page_home';
  $ttag = 'h1';
} else {
  $page_id = get_the_ID();
  $pageid = 'page_'.$page_id;
  $posttype = get_post_type( $page_id );
  $ttag = 'span';
}

/*
<div class="mainmenu">
  <div class="mainwrapper">
    <div class="mainmenu__logo"><a href="<?php echo get_home_url() ?>">
      <?php echo svg_icon('icon-id'); ?>
      <<?php echo $ttag ?>>Title</<?php echo $ttag ?>>
    </a></div>
    <div class="mainmenu__line">
      <?php get_nav() ?>
    </div>
  </div>
</div>
*/
?>

<button class="burgermenu">
  <?php echo svg_icon('icon-burger'); ?>
  <?php echo svg_icon('icon-close'); ?>
</button>

<tryhard>

  <header class="header">
    <div class="mainwrapper">

      <div class="mainmenu">
          <?php echo get_nav() ?>
      </div>

    </div>
  </header>

  <div class="contenu<?php if(!empty($posttype)){ echo ' '.$posttype; } ?>" id="<?php echo $pageid ?>">
