<?php

if (function_exists('is_bbpress') && is_bbpress()) {
  include_once('bbpress/_page.php');
  exit;
}


get_clean_header();
if (have_posts()) : while (have_posts()) : the_post();
?>
<div class="forums">
  <div class="mainwrapper">
PAGE
  <h1><?php the_title() ?></h1>

  <?php the_content(); ?>

  </div>
</div>
<?php 
endwhile; endif;
get_clean_footer();
