<?php
/*
Template Name: Exemple archive avec template
*/
get_clean_header();
$term = get_queried_object();

$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$ppp = get_option( 'posts_per_page' );
$offset = $paged * $ppp - $ppp;

?>

<div class="archive">

  <?php
  if (is_archive()) {

    if (have_posts()) : while (have_posts()) : the_post();
      // display $post
    endwhile; endif;

  } else if (is_page()) {

    $args = array(
      'orderby'          => 'date',
      'order'            => 'DESC',
      'post_type'        => 'post',
      'posts_per_page' => $ppp,
      'offset'          => $offset,
    );
    $actus = get_posts($args);
    if (!empty($actus)) {
      foreach ($actus as $actu) {
        // display $actu
      }
    }

  }
  ?>

  <div class="pagination">
    <?php 

    if (is_page()) {
      $maxpage = ceil(wp_count_posts()->publish / $ppp);
      $baseurl = get_permalink($post->ID);

    } else if (is_archive()) {
      $maxpage = $wp_query->max_num_pages;
      $baseurl = get_term_link($term);

    }
    echo btw_pagination($paged,$maxpage,true,true,$baseurl);
    ?>
  </div>

</div>

<?php
get_clean_footer();