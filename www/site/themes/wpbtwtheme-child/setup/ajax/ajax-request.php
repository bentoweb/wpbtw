<?php

function getCollectivite() {

  // if (defined('DOING_AJAX') && DOING_AJAX) {

  if (!empty($_POST['c_id'])) {
    $ville = get_post($_POST['c_id']);
    if ($ville->post_type=='collectivites') {
      echo json_encode($ville);
    }
  }

  die(); // don't delete !!!

}

add_action( 'wp_ajax_getcollectivite', 'getCollectivite' );
add_action( 'wp_ajax_nopriv_getcollectivite', 'getCollectivite' );
