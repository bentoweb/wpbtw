<?php
// hook into the init action and call create_book_taxonomies when it fires
add_action( 'init', 'create_type_taxonomies', 0 );

// create two taxonomies, genres and writers for the post type "book"
function create_type_taxonomies() {

  // Add new taxonomy, make it hierarchical (like categories)
  $labels = array(
    'name'              => "Types",
    'singular_name'     => "Type",
    'search_items'      => "Rechercher",
    'all_items'         => "Voir tout",
    'parent_item'       => "Parent",
    'parent_item_colon' => "Parent :",
    'edit_item'         => "Modifier",
    'update_item'       => "Mettre à jour",
    'add_new_item'      => "Ajouter",
    'new_item_name'     => "Créer",
    'menu_name'         => "Types",
  );

  $args = array(
    'labels'            => $labels,
    'hierarchical'      => true,
    'show_ui'           => true,
    'show_admin_column' => true,
    'query_var'         => true,
    'rewrite'           => array( 'slug' => '/telechargements', 'with_front' => false ),
  );

  register_taxonomy( 'type', array( 'telechargements' ), $args );

}
