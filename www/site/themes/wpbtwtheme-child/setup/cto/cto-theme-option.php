<?php


/*

		AJOUTER CHAMP CHANGER LOGO ADMIN + CHAMP LOGO FRONT

*/

if (!function_exists('theme_option_page')) {

// Valeur de base
$theme_option = array(
	'google_analytics' => '',
	'google_tag_manager' => '',
	'google_webmaster_tools' => '',
	'facebook' => '',
	'twitter' => '',
	'linkedin' => '',
	'viadeo' => '',
	'couleur' => '',
	'admin_bar_on' => 0,
	'login_page' => 'admin',
	'login_page_ok' => 'admin'
);

if ( is_admin() ) :

	function theme_register_settings() {
		register_setting( 'theme_option', 'theme_option', 'theme_validate_options' );
	}
	add_action( 'admin_init', 'theme_register_settings' );

	function theme_option() {
		add_menu_page( 'Theme options', 'Theme options', 'edit_theme_options', 'options', 'theme_option_page','',4 );
		/*
			2 Dashboard
			4 Separator
			5 Posts
			10 Media
			15 Links
			20 Pages
			25 Comments
			59 Separator
			60 Appearance
			65 Plugins
			70 Users
			75 Tools
			80 Settings
			99 Separator
		*/
	}
	add_action( 'admin_menu', 'theme_option' );

	// La page d'options
	function theme_option_page() {
		global $theme_option;

		if ( ! isset( $_REQUEST['updated'] ) )
			$_REQUEST['updated'] = false; // Check l'update ?>

		<div class="wrap">
			<h2>Theme options</h2>
			<?php
		    $themePath = get_template_directory();
		    $themePath = explode('/',$themePath);
		    $theme_name = end($themePath);
			// $theme_name = 'twentyeleven';
			// $theme_data = get_theme_data( get_theme_root() . '/' . $theme_name . '/style.css' );
			$theme_data = wp_get_theme( $theme_name );

			?>
			<p><b>Theme base v<?php echo $theme_data['Version']; ?></p>

			<form method="post" action="options.php">
					<?php $settings = get_option( 'theme_option' ); ?>
					<?php settings_fields( 'theme_option' ); ?>

					<table class="form-table">
						<tr><th scope="row"><h3>Google Tracking</h3></th></tr>
						<tr><th scope="row"><label for="google_analytics">Google Analytics :</label></th>
							<td>
								<input style="width:100px" id="google_analytics" placeholder="GA-XXXXXXX-XX" name="theme_option[google_analytics]" type="text" value="<?php  esc_attr_e($settings['google_analytics']); ?>" />
							</td>
						</tr>
						<tr><th scope="row"><label for="google_tag_manager">Google Tag Manager :</label></th>
							<td>
								<input style="width:100px" id="google_tag_manager" placeholder="GTM-XXXXXX" name="theme_option[google_tag_manager]" type="text" value="<?php  esc_attr_e($settings['google_tag_manager']); ?>" />
							</td>
						</tr>
						<tr><th scope="row"><label for="google_webmaster_tools">Google Webmaster Tools :</label></th>
							<td>
								<input style="width:400px" id="google_webmaster_tools" placeholder="XXXXXX-XXXXXX-XXXXXXXXXXXXXXXXXXXXXX-XXXXXXX" name="theme_option[google_webmaster_tools]" type="text" value="<?php  esc_attr_e($settings['google_webmaster_tools']); ?>" />
							</td>
						</tr>
						<tr><th scope="row"><h3>Réseaux sociaux</h3></th></tr>
						<tr><th scope="row"><label for="facebook">facebook :</label></th>
							<td>
								<input style="width:400px" id="facebook" name="theme_option[facebook]" type="text" value="<?php  esc_attr_e($settings['facebook']); ?>" />
							</td>
						</tr>
						<tr><th scope="row"><label for="googleplus">google+ :</label></th>
							<td>
								<input style="width:400px" id="googleplus" name="theme_option[googleplus]" type="text" value="<?php  esc_attr_e($settings['googleplus']); ?>" />
							</td>
						</tr>
						<tr><th scope="row"><label for="twitter">twitter :</label></th>
							<td>
								<input style="width:400px" id="twitter" name="theme_option[twitter]" type="text" value="<?php  esc_attr_e($settings['twitter']); ?>" />
							</td>
						</tr>
						<tr><th scope="row"><label for="linkedin">linkedin :</label></th>
							<td>
								<input style="width:400px" id="linkedin" name="theme_option[linkedin]" type="text" value="<?php  esc_attr_e($settings['linkedin']); ?>" />
							</td>
						</tr>
						<tr><th scope="row"><label for="viadeo">viadeo :</label></th>
							<td>
								<input style="width:400px" id="viadeo" name="theme_option[viadeo]" type="text" value="<?php  esc_attr_e($settings['viadeo']); ?>" />
							</td>
						</tr>
						<tr><th scope="row"><label for="instagram">instagram :</label></th>
							<td>
								<input style="width:400px" id="instagram" name="theme_option[instagram]" type="text" value="<?php  esc_attr_e($settings['instagram']); ?>" />
							</td>
						</tr>
						<tr><th scope="row"><label for="pinterest">pinterest :</label></th>
							<td>
								<input style="width:400px" id="pinterest" name="theme_option[pinterest]" type="text" value="<?php  esc_attr_e($settings['pinterest']); ?>" />
							</td>
						</tr>
						<tr><th scope="row"><label for="youtube">youtube :</label></th>
							<td>
								<input style="width:400px" id="youtube" name="theme_option[youtube]" type="text" value="<?php  esc_attr_e($settings['youtube']); ?>" />
							</td>
						</tr>
						<tr><th scope="row"><label for="color">Couleur principale :</label></th>
							<td>
								<?php
							  $couleur = $settings['couleur'];
							  if (empty($couleur)) {$couleur='CCCCCC';}
							  ?>
							  <input id="color" class="color" type="text" style="width:100px;" name="theme_option[couleur]" value="<?php echo $couleur; ?>">
							</td>
						</tr>
						<tr><th scope="row"><label for="color">Couleur des boutons :</label></th>
							<td>
								<?php
							  $couleur_boutons = $settings['couleur_boutons'];
							  if (empty($couleur_boutons)) {$couleur_boutons='0091CD';}
							  ?>
							  <input id="color" class="color" type="text" style="width:100px;" name="theme_option[couleur_boutons]" value="<?php echo $couleur_boutons; ?>">
							</td>
						</tr>
						<tr><th scope="row"><label for="color">Couleur du texte des boutons :</label></th>
							<td>
								<?php
							  $couleur_texte_boutons = $settings['couleur_texte_boutons'];
							  if (empty($couleur_texte_boutons)) {$couleur_texte_boutons='FFFFFF';}
							  ?>
							  <input id="color" class="color" type="text" style="width:100px;" name="theme_option[couleur_texte_boutons]" value="<?php echo $couleur_texte_boutons; ?>">
							</td>
						</tr>
					</table>

					<div>
						<h3>Afficher la barre d'outils</h3>
						<?php
							if (!empty($settings['toolbar_front'])) {
							  $toolbar_front = $settings['toolbar_front'];
							} else {
							  $toolbar_front = 'off';
							}
							if (!empty($settings['toolbar_back'])) {
							  $toolbar_back = $settings['toolbar_back'];
							} else {
							  $toolbar_back = 'off';
							}
							// if (empty($toolbar_front)) { $toolbar_front = 'false'; }
						 //  $toolbar_back = $settings['toolbar_back'];
							// if (empty($toolbar_back)) { $toolbar_back = 'false'; }
						?>
						<div>
							<input name="theme_option[toolbar_front]" id="toolbar_front" type="checkbox" value="on" <?php if ($toolbar_front=='on') {echo 'checked';} ?>><label for="toolbar_front"> Front</label>
						</div>
						<div>
							<input name="theme_option[toolbar_back]" id="toolbar_back" type="checkbox" value="on" <?php if ($toolbar_back=='on') {echo 'checked';} ?>><label for="toolbar_back"> Back</label>
						</div>
					</div>

					<div>
						<h3>Page de login</h3>
						<input style="width:200px" id="login_page" name="theme_option[login_page]" type="text" value="<?php if (!empty($settings['login_page'])) { echo $settings['login_page']; } else { echo 'admin'; } ?>" />
						<input id="login_page_ok" name="theme_option[login_page_ok]" type="hidden" value="<?php if (!empty($settings['login_page_ok'])) { echo $settings['login_page_ok']; } else { echo 'admin'; } ?>" />
					</div>

					<p class="submit"><input type="submit" class="button button-primary button-large" value="Sauvegarder" /></p>
			</form>

		</div>
		<div>
			<a class="button" href="<?php echo get_bloginfo('url') ?>/wp-admin/themes.php">Voir les thèmes</a>
			<a class="button" href="<?php echo get_bloginfo('url') ?>/wp-admin/widgets.php">Voir les widgets</a>
		</div>

		<?php
	}

	function theme_validate_options( $input ) {
		global $theme_option;
		// $settings = get_option( 'theme_option', $theme_option );

		//Sauvegarde
		$input['google_analytics'] = wp_filter_nohtml_kses( $input['google_analytics'] );
		$input['google_tag_manager'] = wp_filter_nohtml_kses( $input['google_tag_manager'] );
		$input['google_webmaster_tools'] = wp_filter_nohtml_kses( $input['google_webmaster_tools'] );
		$input['facebook'] = wp_filter_nohtml_kses( $input['facebook'] );
		$input['googleplus'] = wp_filter_nohtml_kses( $input['googleplus'] );
		$input['twitter'] = wp_filter_nohtml_kses( $input['twitter'] );
		$input['linkedin'] = wp_filter_nohtml_kses( $input['linkedin'] );
		$input['viadeo'] = wp_filter_nohtml_kses( $input['viadeo'] );
		$input['instagram'] = wp_filter_nohtml_kses( $input['instagram'] );
		$input['pinterest'] = wp_filter_nohtml_kses( $input['pinterest'] );
		$input['youtube'] = wp_filter_nohtml_kses( $input['youtube'] );

		$input['couleur'] = wp_filter_nohtml_kses( $input['couleur'] );
		$input['couleur_boutons'] = wp_filter_nohtml_kses( $input['couleur_boutons'] );
		$input['couleur_texte_boutons'] = wp_filter_nohtml_kses( $input['couleur_texte_boutons'] );

		// $input['admin_bar_on'] = wp_filter_nohtml_kses( $input['admin_bar_on'] );
		$input['login_page'] = alias( $input['login_page'] );
		// $input['login_page_ok'] = alias( $input['login_page_ok'] );

		// echo $input['login_page'];
		// echo $input['login_page_ok'];
		// exit;

		if ( ($input['login_page']!=$input['login_page_ok']) && !empty($input['login_page']) ) {
			$loginpage = array(
			  'ID'        => 5,
			  'post_name' => $input['login_page']
			);
			wp_update_post( $loginpage );
			$input['login_page_ok'] = $input['login_page'];
		}

		return $input;
	}

endif; // if admin

/* POUR RÉCUPÉRER LES VALEURS SUR UNE PAGE
	<?php global $theme_option; $theme_settings = get_option( 'theme_option', $theme_option ); ?>
	<?php if( $theme_settings['google_analytics'] != '' ) : echo $theme_settings['google_analytics']; endif; ?>
	<?php if( $theme_settings['google_tag_manager'] != '' ) : echo $theme_settings['google_tag_manager']; endif; ?>
	<?php if( $theme_settings['google_webmaster_tools'] != '' ) : echo $theme_settings['google_webmaster_tools']; endif; ?>
	<?php if( $theme_settings['facebook'] != '' ) : echo $theme_settings['facebook']; endif; ?>
	<?php if( $theme_settings['twitter'] != '' ) : echo $theme_settings['twitter']; endif; ?>
	<?php if( $theme_settings['linkedin'] != '' ) : echo $theme_settings['linkedin']; endif; ?>
	<?php if( $theme_settings['viadeo'] != '' ) : echo $theme_settings['viadeo']; endif; ?>
*/

}
