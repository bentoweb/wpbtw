<?php
function register_cpt_creches() {

  $labels = array(
    'name' => "Crèches",
    'singular_name' => "Crèche",
    'add_new' => "Ajouter",
    'add_new_item' => "Ajouter",
    'edit_item' => "Editer",
    'new_item' => "Créer",
    'view_item' => "Afficher",
    'search_items' => "Rechercher",
    'not_found' => "Aucun résultat",
    'not_found_in_trash' => "Corbeille vide",
    'parent_item_colon' => "Parent",
    'menu_name' => "Crèches",
  );

  $args = array(
    'labels' => $labels,
    'hierarchical' => false,
    'supports' => array( 'title', 'thumbnail' ),
    //'taxonomies' => array( 'category', 'post_tag' ),
    'public' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'menu_position' => 5,
    'menu_icon' => 'dashicons-buddicons-groups',
    // Voir la liste => http://melchoyce.github.io/dashicons/
    'show_in_nav_menus' => true,
    'publicly_queryable' => false,
    'exclude_from_search' => false,
    'has_archive' => false,
    'query_var' => true,
    'can_export' => true,
    'rewrite' => false,
    'capability_type' => 'post',
    // 'rewrite' => array( 'slug' => ('clients') ),
  );

  register_post_type( 'creches', $args );
}
add_action( 'init', 'register_cpt_creches' );
