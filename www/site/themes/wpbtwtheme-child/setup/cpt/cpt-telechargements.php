<?php
function register_cpt_telechargements() {

  $labels = array(
    'name' => "Fichiers",
    'singular_name' => "Fichier",
    'add_new' => "Ajouter",
    'add_new_item' => "Ajouter",
    'edit_item' => "Modifier",
    'new_item' => "Ajout",
    'view_item' => "Voir",
    'search_items' => "Rechercher",
    'not_found' => "Aucun résultat",
    'not_found_in_trash' => "Aucun résultat dans la corbeille",
    'parent_item_colon' => "Parent",
    'menu_name' => "Telechargements",
  );

  $args = array(
    'labels' => $labels,
    'hierarchical' => false,
    'supports' => array( 'title' ),
    //'taxonomies' => array( 'category', 'post_tag' ),
    'public' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'menu_position' => 5,
    'menu_icon' => 'dashicons-media-document',
    // Voir la liste => http://melchoyce.github.io/dashicons/
    'show_in_nav_menus' => true,
    'publicly_queryable' => true,
    'exclude_from_search' => false,
    'has_archive' => true,
    'query_var' => true,
    'can_export' => true,
    // 'rewrite' => false,
    // 'rewrite' => true,
    'capability_type' => 'post',
    'rewrite' => array( 'slug' => ('fichiers'), 'with_front' => false ), // Apparemment "telechargements" est un slug interdit ?
    // 'rewrite' => array( 'slug' => ('telechargements'), 'with_front' => false ),
  );

  register_post_type( 'telechargements', $args );
}
add_action( 'init', 'register_cpt_telechargements' );



/*
function btw_change_telechargements_posts_per_page( $query ) {
  if( $query->is_main_query() && !is_admin() && is_post_type_archive( 'telechargements' ) ) {
    $query->set( 'posts_per_page', '15' );
  }
}
add_action( 'pre_get_posts', 'be_change_event_posts_per_page' );
*/






add_filter('acf/upload_prefilter/name=fichier_prive', 'field_name_upload_prefilter');
function field_name_upload_prefilter($errors) {
  // in this filter we add a WP filter that alters the upload path
  add_filter('upload_dir', 'field_name_upload_dir');
  return $errors;
}
// second filter
function field_name_upload_dir($uploads) {
  // here is where we later the path
  $custompath = '/private';
  $uploads['path'] = $uploads['basedir'].$custompath;
  $uploads['url'] = $uploads['baseurl'].$custompath;
  $uploads['subdir'] = '';
  return $uploads;
}