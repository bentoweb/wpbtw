<?php 
/*
*/
Class BlockBuilder {
  private static $blockbuilders = array();

  public static function addBlockBuilder($key=null,$method=null) {
    if (empty($key) || empty($method)) { return; }
    self::$blockbuilders[$key] = $method;
  }

  public static function doBlock($block) {
    if (empty($block) || empty(self::$blockbuilders)) { return; }
    return self::$blockbuilders[$block['acf_fc_layout']]($block);
  }
}

/*
TO ADD A NEW BLOCK TYPE :
BlockBuilder::addBlockBuilder('editeur',function($block){
  // var_dump($block);
  $html = '<p>CLASS: test 3 EDITEUR ok : '.$block['acf_fc_layout'].'</p>';
  return $html;
});
*/