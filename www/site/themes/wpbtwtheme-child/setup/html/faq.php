<?php 

BlockBuilder::addBlockBuilder('faq',function($block){
  // var_dump($block);
  $html = '';

  $faqclass = '';
  // if (!empty($block['theme']) && $block['theme']=='dark') {
  //   $faqclass .= ' m--dark'; 
  // }
    
  $html .= '
  <div class="pagebloc__faq '.$faqclass.'">
  ';
  if (!empty($block['questions'])) {

    $html .= '
    <div class="faq">
    ';

    foreach ($block['questions'] as $question) {
      $html .= '
      <div class="faq__question">
        <button class="faq__close m--pseudoclose" type="button"></button>
        <h3 class="faq__openbutton"><span>'.$question['question'].'</span></h3>
        <div class="faq__reponse">
          '.wpautop($question['reponse']).'
        </div>
        <div class="liens">
      ';
      if (!empty($question['reponse']['liens'])) {
        foreach ($question['reponse']['liens'] as $lien) {
          $html .= '
            <a href="'.$lien['url'].'" title="'.$lien['title'].'" target="'.$lien['target'].'">'.$lien['title'].'</a>
          ';
        }
      }
      $html .= '
        </div>
      </div>
      ';
    }

    $html .= '
    </div>
    ';
  }

  $html .= '
  </div><!-- .pagebloc__faq -->';

  return $html;
});