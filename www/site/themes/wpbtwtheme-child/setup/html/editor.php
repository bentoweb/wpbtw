<?php 

BlockBuilder::addBlockBuilder('editeur',function($block){
  $html = '';
  // var_dump($block);

  $html .= '<div class="mce-content-body">';
  if (!empty($block['titre'])) {
    $html .= '<h2>'.$block['titre'].'</h2>';
  }
  if (!empty($block['texte'])) {
    $html .= wpautop($block['texte']);
  }
  $html .= '</div>';

  return $html;
});


