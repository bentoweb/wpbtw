<?php 

BlockBuilder::addBlockBuilder('colonnes',function($block){
  $html = '';
  // var_dump($block);

  $nbrc = count($block['colonnes']);

  $innerstyle = '';
  $colstyle = '';
  // $outerstyle = '';
  if (!empty($block['largeur_colonne'])) {
    // $padding = 100 - intval($block['largeur_colonne']);
    // $innerstyle = ' padding-left: '.$padding.'%; padding-right: '.$padding.'%;';
    // $outerstyle = ' margin-left: -'.$padding.'%; margin-right: -'.$padding.'%;';
    // $colstyle = 'width: '.intval($block['largeur_colonne']).'%';
    $colstyle = 'width: '.(floor( floatval(100/$nbrc) / 100 * floatval($block['largeur_colonne']) * 10 ) / 10).'%';
  } else {
    $colstyle = 'width: '.(floor( floatval(100/$nbrc) * 10 ) / 10).'%';
  }


  $html .= '
  <div class="pagebloc__columns">';
  foreach($block['colonnes'] as $colonne) {
    $itemclass = '';

    $bouton_de_colonne = $colonne['bouton_bas_de_colonne'];
    if (!empty($bouton_de_colonne)) {
      $itemclass .= ' m--buttoned';
    }

    $html .= '
    <div class="pagebloc__columns__item '.$itemclass.'" style="'.$colstyle.'">
      <div class="pagebloc__columns__inner '.$itemclass.'" style="'.$innerstyle.'">';

      $col_contents = $colonne['type_de_contenu'];
      foreach ($col_contents as $contenu) {

        if ($contenu['acf_fc_layout']=='image') {
          $img = getImageObj($contenu['image'],800,0,80,false,false);
          $html .= '
          <div class="image">
            <img src="'.$img->src.'" alt="'.$img->post_title.'">
          </div>';
        } else 

        if ($contenu['acf_fc_layout']=='editeur') {
          $html .= '
          <div class="texte mce-content-body">
            '.wpautop($contenu['editeur_de_texte']).'
          </div>';
        }

      }

      if (!empty($bouton_de_colonne)) {
        // pr($bouton_de_colonne);
        $html .= '
        <div class="colbottom">
          <a class="colbutton" href="'.$bouton_de_colonne['url'].'" title="'.$bouton_de_colonne['title'].'" target="'.$bouton_de_colonne['target'].'">'.$bouton_de_colonne['title'].'</a>
        </div>
        ';
      }

      $html .= '
      </div><!-- .pagebloc__columns__inner -->
    </div><!-- .pagebloc__columns__item -->
    ';
  }
// }


  $html .= '
  </div><!-- .pagebloc__columns -->';
  
  return $html;
});


