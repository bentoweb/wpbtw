<?php

/*
  Custom Fonts
*/
// function custom_add_google_fonts() {
  // wp_enqueue_style( 'custom-google-fonts-', 'https://fonts.googleapis.com/css?family=Barlow+Condensed:300,400,500,700|Nunito:400,600,700&display=swap', false );
// }
// add_action( 'wp_enqueue_scripts', 'custom_add_google_fonts' );



function custom_add_myscripts() {
  if( !is_admin()){

  //////// JQUERY
  wp_deregister_script('jquery');
  wp_register_script('jquery','https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js', false, '');
  wp_enqueue_script('jquery');
  // wp_enqueue_script( 'jquery' );
  }

  //////// LEAFLET
  /*
  wp_register_script('leaflet','https://unpkg.com/leaflet@1.7.1/dist/leaflet.js', false, '');
  wp_enqueue_script('leaflet');
  }
  wp_enqueue_style( 'leaflet', 'https://unpkg.com/leaflet@1.7.1/dist/leaflet.css', false );
  */

}

add_action( 'wp_enqueue_scripts', 'custom_add_myscripts' );




add_action('admin_head', 'my_admin_custom_styles');
function my_admin_custom_styles() {
  wp_enqueue_style( 'admincustomcss', get_home_url().'/assets/css/admin.css', false );
}



/*
  Custom CSS
*/
// function custom_add_customformulairecss() {
  // wp_enqueue_style( 'customformulaire', get_home_url().'/assets/formulaire.css', false );
// }
// add_action( 'wp_enqueue_scripts', 'custom_add_customformulairecss' );




register_nav_menus( array(
  'menu_header' => 'Menu Header',
  'menu_footer' => 'Menu Footer'
) );





/*
  Preload fonts
  Avoid flash of text
*/
/*
add_action('wp_head' , function(){
  echo'
    <link rel="preload" href="'.assets().'fonts/CocogoosePro-Regular.otf" as="font" crossorigin="anonymous">
    <link rel="preload" href="'.assets().'fonts/CocogoosePro-Light.otf" as="font" crossorigin="anonymous">
    <link rel="preload" href="'.assets().'fonts/CocogoosePro-Thin.otf" as="font" crossorigin="anonymous">
    <link rel="preload" href="'.assets().'fonts/CocogoosePro-Inline.otf" as="font" crossorigin="anonymous">
    <link rel="preload" href="'.assets().'fonts/zingrust-base.otf" as="font" crossorigin="anonymous">
  ';
});
*/







/*
https://perishablepress.com/disable-wordpress-generated-images/
*/
// disable generated image sizes
function shapeSpace_disable_image_sizes($sizes) {
  
  // unset($sizes['thumbnail']);    // disable thumbnail size
  unset($sizes['medium']);       // disable medium size
  unset($sizes['medium_large']); // disable medium-large size
  unset($sizes['large']);        // disable large size
  unset($sizes['1536x1536']);    // disable 2x medium-large size
  unset($sizes['2048x2048']);    // disable 2x large size
  
  return $sizes;
  
}
add_action('intermediate_image_sizes_advanced', 'shapeSpace_disable_image_sizes');

// disable scaled image size
add_filter('big_image_size_threshold', '__return_false');

// disable other image sizes
function shapeSpace_disable_other_image_sizes() {
  
  // remove_image_size('post-thumbnail'); // disable images added via set_post_thumbnail_size() 
  // remove_image_size('another-size');   // disable any other added image sizes
  
}
add_action('init', 'shapeSpace_disable_other_image_sizes');