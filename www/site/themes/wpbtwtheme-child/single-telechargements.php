<?php 

// $fichier = get_field('fichier');





$fileoption = get_field('fileoption');

if (!empty($fileoption)) {
  /*
  PRIVATE
  Rajouter ici des conditions d'accès au fichier si privé, comme un rôle d'utilisateur ou autre.
  Sinon : exit ou redirect.
  */
  $fichier = get_field('fichier_prive')
  ;
} else {
  /*
  PUBLIC
  */
  $fichier = get_field('fichier');

}

$path = ABSPATH.'medias/'.$fichier['filename'];

header('Content-type: '.$fichier['mime_type'],true,200);
header("Content-Disposition: attachment; filename=".$fichier['filename']);
header('Cache-Control: public');
readfile($path);

exit;
