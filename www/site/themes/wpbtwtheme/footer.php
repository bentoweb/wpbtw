	<?php

	// $themePath = get_stylesheet_directory();
	// $themePath = explode('/',$themePath);
	// $themePath = end($themePath);
	require_once(dirname(__FILE__).'/../'.WP_DEFAULT_THEME.'/htmlfooter.php');

	wp_footer();

	?>



<?php 
// BTWFLAGS

if (is_user_logged_in()) {

	$flags = '';

	if (WP_DEBUG) {
		$flags .= '<div class="btwflags__item flag__dbg"><span>Debug</span></div>';
	}

	$mm = get_query_var( 'btw-maintenance-mode', true );
	if ($mm) {
		$flags .= '<div class="btwflags__item flag__mm"><span>Maintenance</span></div>';
	}

	if (!empty($flags)) {
		echo '<div class="btwflags">'.$flags.'</div>';
	}

}

?>
</body>
</html>
