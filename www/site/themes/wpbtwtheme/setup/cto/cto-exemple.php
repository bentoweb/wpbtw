<?php

// Valeur de base
$nom_de_la_page_d_option = array(
	'titre' => '',
	'texte' => ''
);

if ( is_admin() ) :

function ocp_register_settings() {
	register_setting( 'nom_de_la_page_d_option', 'nom_de_la_page_d_option', 'ocp_validate_options' );
}
add_action( 'admin_init', 'ocp_register_settings' );

function nom_de_la_page_d_option() {
	add_menu_page( 'Nom de la page', 'Nom de la page', 'edit_theme_options', 'statistiques', 'nom_de_la_page_d_option_page','',4 );
	//add_menu_page('Nom de la page', 'Nom dans le menu', 'permissions', 'slug', 'function',' ico url ', place dans le menu* );
	//*place dans le menu :
		// 2 Dashboard
		// 4 Separator
		// 5 Posts
		// 10 Media
		// 15 Links
		// 20 Pages
		// 25 Comments
		// 59 Separator
		// 60 Appearance
		// 65 Plugins
		// 70 Users
		// 75 Tools
		// 80 Settings
		// 99 Separator
}
add_action( 'admin_menu', 'nom_de_la_page_d_option' );

// La page d'options
function nom_de_la_page_d_option_page() {
	global $nom_de_la_page_d_option;

	if ( ! isset( $_REQUEST['updated'] ) )
		$_REQUEST['updated'] = false; // Check l'update ?>

	<div class="wrap">

	<h2>Statistiques du site (formations, projets, cours...)</h2>

	<form method="post" action="options.php">
	<?php $settings = get_option( 'nom_de_la_page_d_option', $nom_de_la_page_d_option ); ?>
	<?php settings_fields( 'nom_de_la_page_d_option' ); ?>

	<table class="form-table">

	<tr valign="top"><th scope="row"><label for="titre">Titre :</label></th>
	<td>
	<input id="titre" name="nom_de_la_page_d_option[titre]" type="text" value="<?php  esc_attr_e($settings['titre']); ?>" />
	</td>
	</tr>

	<tr valign="top"><th scope="row"><label for="texte">Texte :</label></th>
	<td>
	<textarea id="texte" name="nom_de_la_page_d_option[texte]" rows="5" cols="30"><?php echo stripslashes($settings['texte']); ?></textarea>
	</td>
	</tr>

	</table>

	<p class="submit"><input type="submit" class="button-primary" value="Save Options" /></p>

	</form>

	</div>

	<?php
}

function ocp_validate_options( $input ) {
	global $nom_de_la_page_d_option;
	$settings = get_option( 'nom_de_la_page_d_option', $nom_de_la_page_d_option );
	
	//Sauvegarde
	$input['titre'] = wp_filter_nohtml_kses( $input['titre'] );
	
	$input['texte'] = wp_filter_post_kses( $input['texte'] );
	
	return $input;
}

endif; // if admin


/* POUR RÉCUPÉRER LES VALEURS SUR UNE PAGE

<?php global $nom_de_la_page_d_option; $ocp_settings = get_option( 'nom_de_la_page_d_option', $nom_de_la_page_d_option ); ?>
<?php if( $ocp_settings['titre'] != '' ) : echo $ocp_settings['titre']; endif; ?>  
<?php if( $ocp_settings['texte'] != '' ) : echo $ocp_settings['texte']; endif; ?>  

*/

