<?php

add_theme_support('root-relative-urls');    // Enable relative URLs
add_theme_support('rewrites');              // Enable URL rewrites
add_theme_support('post-thumbnails');   // Enable thumnail
// add_theme_support('nice-search');           // Enable /?s= to /search/ redirect
// add_theme_support('jquery-cdn');            // Enable to load jQuery from the Google CDN
// add_filter('show_admin_bar', '__return_false');

remove_action('template_redirect', 'wp_old_slug_redirect');

function iwantuse_post_thumbnails() {
  add_theme_support( 'post-thumbnails' );
}
add_action( 'after_setup_theme', 'iwantuse_post_thumbnails' );

/*
register_nav_menus( array(
  'menu_header' => 'Menu Header',
  'menu_footer' => 'Menu Footer'
) );
*/





function roots_scripts(){

  wp_deregister_script( 'wp-embed' );

  if( !is_admin()){
    wp_deregister_script('jquery');
    wp_enqueue_script('jquery', get_home_url().'/parent/assets/js/libs/jquery-3.6.0.min.js', array(), null, true);
  }

  //-- Child
  if (WP_DEBUG) {
    $v = time();
    $filename = 'output-dev';
  } else {
    $v = wp_get_theme()->version;
    $filename = 'output';
  }
  wp_register_script('main-js', '/assets/'.$filename.'.js?v='.$v, array('jquery'), '', true);
  wp_enqueue_script('main-js');

  // AJAX //
  wp_localize_script('main-js', 'ajax_var', array(
    'url' => home_url().'/ajax/'
  ));

}
add_action('wp_enqueue_scripts', 'roots_scripts', 100);


// function admin_scripts(){
// }
// add_action('admin_enqueue_scripts', 'admin_scripts', 100);







// remove wp version number from scripts and styles
// remove version from head
remove_action('wp_head', 'wp_generator');

// remove version from rss
add_filter('the_generator', '__return_empty_string');

// remove version from scripts and styles
function remove_version_scripts_styles($src) {
  if (strpos($src, 'ver=')) {
    $src = remove_query_arg('ver', $src);
  }
  return $src;
}
add_filter('style_loader_src', 'remove_version_scripts_styles', 9999);
add_filter('script_loader_src', 'remove_version_scripts_styles', 9999);




function stylesheet_uri_filter($stylesheet_uri, $stylesheet_dir_uri) {
  if (WP_DEBUG) {
    $v = time();
    return get_home_url().'/assets/output-dev.css?v='.$v;
  } else {
    $v = wp_get_theme()->version;
    return get_home_url().'/assets/output.css?v='.$v;
  }
}
add_filter('stylesheet_uri', 'stylesheet_uri_filter', 10, 2);








add_action('init', 'stop_heartbeat', 1);
function stop_heartbeat () {
  wp_deregister_script('heartbeat');
}



