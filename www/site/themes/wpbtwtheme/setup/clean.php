<?php

function get_clean_header(){
  // ob_start();
  get_header();
  // $content = ob_get_contents();
  // Supprime tous les commentaires HTML sauf ceux commençant par "<!--[" pour gardes les condtionnels IE
  // $content = preg_replace('/<!--[^\[](.|s)*?-->/', '', $content);
  // ob_end_clean();
  // echo $content;
}

function get_clean_footer(){
  // ob_start();
  get_footer();
  // $content = ob_get_contents();
  // Supprime tous les commentaires HTML sauf ceux commençant par "<!--[" pour gardes les condtionnels IE
  // $content = preg_replace('/<!--[^\[](.|s)*?-->/', '', $content);
  // ob_end_clean();
  // echo $content;
}


add_action( 'wp_print_styles', 'wps_deregister_styles', 100 );
function wps_deregister_styles() {
    wp_dequeue_style( 'wp-block-library' );
}


remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('admin_print_scripts', 'print_emoji_detection_script');
remove_action('wp_print_styles', 'print_emoji_styles');
remove_action('admin_print_styles', 'print_emoji_styles');

remove_action('wp_head','rest_output_link_wp_head');
remove_action('wp_head','wp_oembed_add_discovery_links');
remove_action('template_redirect', 'rest_output_link_header', 11 );
remove_action( 'wp_head', 'wlwmanifest_link');
remove_action ('wp_head', 'rsd_link');
remove_action( 'wp_head', 'wp_resource_hints', 2 );
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'wp_shortlink_wp_head', 10);
remove_action( 'template_redirect', 'wp_shortlink_header', 11);
remove_action('wp_head', 'rel_canonical');

/**
 * Remove auto correction from Wordpress to WordPress
 */
remove_filter( 'the_title', 'capital_P_dangit', 11 );
remove_filter( 'the_content', 'capital_P_dangit', 11 );
remove_filter( 'comment_text', 'capital_P_dangit', 31 );

/**
 * Disable update info for no admin
 */
function disableupdatesnotices() {
  remove_action('admin_notices', 'update_nag', 3);
}
if (!current_user_can('update_plugins')) {
  remove_action('admin_notices', 'update_nag', 3);
  // add_action('admin_init', 'disableupdatesnotices');
  // add_action('admin_init', create_function(false,"remove_action('admin_notices', 'update_nag', 3);"));
}

// Hide ?author=
// add_filter( 'author_link', 'my_author_link' );
// function my_author_link() {
//   return home_url();
// }




