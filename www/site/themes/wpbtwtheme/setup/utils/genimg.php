<?php 
//----------------- Crée ou récupère une image formatée à la demande
/*
  Exemple :
  $monImageUrl = genImageFromPost(7,800,600,60,true,true,true);
  Si l'image à la une du post 7 est le fichier "Mon_Image.jpg" ayant l'ID 35,
  le fichier généré sera "mon-image-800-600-60-35.jpg".
  Par contre, si dans Médias le titre de l'image est "Mon Titre SEO",
  le nom du fichier sera "mon-titre-seo-800-600-60-35.jpg".

  $ID : ID du post ayant l"image à la une [OU] ID de l'image à produire
  $imgW : Largeur contrainte (laisser 0 pour ne pas contraindre)
  $imgH : Hauteur contrainte (laisser 0 pour ne pas contraindre)
  $qual : Qualité de compression JPG
  $default : Si l'image n'existe pas, utiliser une l'image par défaut (true/false)
  $optimize : true/false
  $avif : true/false
*/

$tkey = get_field('api_key_tinify', 'option');
if (!empty($tkey)) {
  define('TINIFYKEY',$tkey); // Commenter si inexistant
}

if (defined('TINIFYKEY')) {
  require_once(ABSPATH."/tinify-php-master/lib/Tinify/Exception.php");
  require_once(ABSPATH."/tinify-php-master/lib/Tinify/ResultMeta.php");
  require_once(ABSPATH."/tinify-php-master/lib/Tinify/Result.php");
  require_once(ABSPATH."/tinify-php-master/lib/Tinify/Source.php");
  require_once(ABSPATH."/tinify-php-master/lib/Tinify/Client.php");
  require_once(ABSPATH."/tinify-php-master/lib/Tinify.php");
  // \Tinify\setKey('OMXw_rOzUq1M8XgroqqkC4tSFlFvLm_X');
  \Tinify\setKey(TINIFYKEY);
}

// function getbimg($opts=array()) {}

// class Bimg {
    // private static $arr = array(1, 2, 3, 4);
  // public static function bar($var1, $var2, array $var3 = null) {
    // if (is_null($var3)) {
    //     $var3 = self::$arr;
    // }
    // your code here
  // }
// }

// Foo::bar('a', 'b');

function getImageTag($idPost,$imgW=0,$imgH=0,$qual=90,$default=false,$optimize=true,$avif=null) {
  $html = '';
  $imghtml = '';
  if (function_exists('imageavif') && is_null($avif)) {
    $avif = true;
  }


  if ($avif==true) {
    $imgjpg = getImageObj($idPost,$imgW,$imgH,$qual,$default,$optimize,false);
    $imgavif = getImageObj($idPost,$imgW,$imgH,$qual,$default,$optimize,true);

    $imghtml = '
    <picture>
    ';
    if (!empty($imgavif)) {
      $imghtml = '
        <source srcset="'.$imgavif->src.'" type="image/avif">
      ';
    }
    if (!empty($imgjpg)) {
      $imghtml = '
        <img src="'.$imgjpg->src.'" alt="'.$imgjpg->post_title.'">
      ';
    }
    $imghtml = '
    </picture>
    ';

  } else {
    $imgjpg = getImageObj($idPost,$imgW,$imgH,$qual,$default,$optimize,false);
    if (!empty($imgjpg)) {
      $imghtml = '
      <picture>  
        <img src="'.$imgjpg->src.'" alt="'.$imgjpg->post_title.'">
      </picture>
      ';
    }

  }

  $html = '
    <figure>
      '.$imghtml.'
      <figcaption>'.$imgjpg->post_excerpt.'</figcaption>
    </figure>
  ';

  return $imghtml;
  // return $html;
}

function getImageObj($idPost,$imgW=0,$imgH=0,$qual=90,$default=false,$optimize=true,$avif=null) {
  if (function_exists('imageavif') && is_null($avif)) {
    $avif = true;
  }
  /*
    Si c'est une image, retourne un objet post media + l'url d'une image générée dans '->src'
    Si c'est un post avec thumbnail, retourne la même chose pour le thumbnail
    Si c'est un SVG, retourne un objet post media + la source du svg dans '->src'
  */
  $thispost = get_post($idPost);
  if (is_object($thispost)) {
    if ($thispost->post_type=='attachment') {
      $r = genImage($thispost->ID,$imgW,$imgH,$qual,$default,$optimize,$avif);
      if ($json = json_decode($r)) {
        $thispost->src = $json[0];
        $thispost->svg = $json[1];
      } else {
        $thispost->src = $r;
      }
      return $thispost;
    } else {
      $post_thumbnail_id = get_post_thumbnail_id($idPost);
      if (!empty($post_thumbnail_id)) {
        $imgpost = get_post($post_thumbnail_id);
        if (!empty($imgpost)) {
          // $imgpost->src = genImage($imgpost->ID,$imgW,$imgH,$qual,$default,$optimize);
          $r = genImage($imgpost->ID,$imgW,$imgH,$qual,$default,$optimize,$avif);
          if ($json = json_decode($r)) {
            $imgpost->src = $json[0];
            $imgpost->svg = $json[1];
          } else {
            $imgpost->src = $r;
          }
        }
        return $imgpost;
      } else {
        if ($default==false) {
          return false;
        } else {
          if (is_int($default)) {
            $imgpost = get_post($default);
            if (is_object($imgpost)) {
              // $imgpost->src = genImage($imgpost->ID,$imgW,$imgH,$qual,$default,$optimize);
              $r = genImage($imgpost->ID,$imgW,$imgH,$qual,$default,$optimize,$avif);
              if ($json = json_decode($r)) {
                $imgpost->src = $json[0];
                $imgpost->svg = $json[1];
              } else {
                $imgpost->src = $r;
              }
              return $imgpost;
            } else {
              return false;
            }
          } else {
            $imgpost = new stdClass();
            $imgpost->src = picsum($imgW,$imgH);
            $imgpost->ID = 0;
            $imgpost->post_title = 'PICSUM PLACEHOLDER';
          }
          return $imgpost;
        }
      }
    }
  } else {
    // Not an object
    return false;
  }
}

function genImage($idImage,$imgW=0,$imgH=0,$qual=90,$default=false,$optimize=true,$avif=null) {
  // $id_defaut_image = 0;
  // if (empty($imgH)&&empty($imgW)) { $imgW = 100; }

  // if( !empty($idImage) && (!empty($imgW) || !empty($imgH)) ){
  if (!empty($idImage)){

    $attImage = wp_get_attachment_image_src( $idImage, 'full' );

    if (empty($attImage)) {
      // if ($default==true) {
      //   $attImage = wp_get_attachment_image_src( $id_defaut_image, 'full' );
      // } else {
        return false;
      // }
    }

    if (empty($imgW) && empty($imgH)) {
      $imgW = $attImage[1];
      $imgH = $attImage[2];
    }

    $decalageX = 0;
    $decalageY = 0;

    $info = pathinfo($attImage[0]);
    $domain = baseurl(get_bloginfo('url'));
    $basename = $info['filename'];
    $extension = $info['extension'];
    $imagepost = get_post($idImage);
    if (empty($imagepost->post_title)) {
      $seotitle = alias($basename);
    } else {
      $seotitle = alias($imagepost->post_title);
    }

    //------ SVG
    if ($extension=='svg') {
      $ext = '.svg';
      $abspath = basepath();
      $imgpath = str_replace('//','/',$abspath.'/images/');
      $filenameext = $seotitle.'-'.$idImage.$ext;

      if(!file_exists($imgpath.$filenameext)) {

        $svgpath = basepath().rw_remove_root($attImage[0]);
        $iconfile = new DOMDocument();
        $iconfile->load($svgpath);
        foreach ($iconfile->getElementsByTagName('title') as $titletag) {
          $titletag->parentNode->removeChild($titletag);
        }
        $content = $iconfile->saveHTML($iconfile->getElementsByTagName('svg')[0]);
        file_put_contents($imgpath.$filenameext, urldecode($content));

      } else {
        $iconfile = new DOMDocument();
        $iconfile->load($imgpath.$filenameext);
        $content = $iconfile->saveHTML($iconfile->getElementsByTagName('svg')[0]);
      }
      
      $domain = baseurl( get_bloginfo('url') );
      $svgsrc = array(
        $domain.'/images/'.$filenameext,
        $content
      );
      return json_encode($svgsrc);
      // return $domain.'/images/'.$filenameext;

    } else if ($extension=='png') {
      $ext = '.png';
      $qualtxt = '';

    } else if ($extension=='gif') {
      $ext = '.gif';
      $qualtxt = '';

    } else {
      $ext = '.jpg';
      $qualtxt = '-'.$qual;

    }

    if ($avif==true) {
      $ext = '.avif';
    }

    $largeurImgBig = $attImage[1];
    $hauteurImgBig = $attImage[2];

    if ($largeurImgBig + $hauteurImgBig <=0) {
      return false;
    }

    // Si seule la largeur est contrainte, on adapte la hauteur proportionnellement
    if($imgW==0){
      $hauteurFinale = $imgH;
      $largeurFinale = floor($hauteurFinale/$hauteurImgBig*$largeurImgBig);
      $hauteurImgSmall = $hauteurFinale;
      $largeurImgSmall = $largeurFinale;
    }
    // Si seule la hauteur est contrainte, on adapte la largeur proportionnellement
    if($imgH==0){
      $largeurFinale = $imgW;
      $hauteurFinale = floor($largeurFinale/$largeurImgBig*$hauteurImgBig);
      $hauteurImgSmall = $hauteurFinale;
      $largeurImgSmall = $largeurFinale;
    }
    // Si largeur et hauteur sont contraintes, on remplit et on crop
    if($imgH!=0 && $imgW!=0){
      $RatioW = $imgW / $largeurImgBig;
      $RatioH = $imgH / $hauteurImgBig;
      $largeurFinale = $imgW;
      $hauteurFinale = $imgH;
      if($RatioW>$RatioH){
        // adapter sur la largeur
        $hauteurImgSmall = floor($hauteurImgBig/$largeurImgBig*$imgW);
        $largeurImgSmall = $largeurFinale;
        $decalageY = floor(($hauteurImgSmall-$imgH)/2);
      } else {
        // adapter sur la hauteur
        $hauteurImgSmall = $hauteurFinale;
        $largeurImgSmall = floor($largeurImgBig/$hauteurImgBig*$imgH);
        $decalageX = floor(($largeurImgSmall-$imgW)/2);
      }
    }

    $filename = $seotitle.'-'.$largeurFinale.'-'.$hauteurFinale.$qualtxt.'-'.$idImage;
    $filenameext = $seotitle.'-'.$largeurFinale.'-'.$hauteurFinale.$qualtxt.'-'.$idImage.$ext;
    $abspath = basepath();
    $imgpath = str_replace('//','/',$abspath.'/images/');

    if(!file_exists($imgpath.$filenameext)) {
      if(!file_exists($imgpath.utf8_decode($filenameext))) {

        if (!file_exists($abspath.'/images')) {
          mkdir($abspath.'/images', 0755, true);
        }

        if ($ext=='jpg') {
          $pel_input_jpeg = new PelJpeg($filenameext);
        }

        $nouvelleImage = imagecreatetruecolor($largeurFinale, $hauteurFinale);
        $imgRelativeSrc = $abspath.rw_remove_root($attImage[0]);
        if(!file_exists($imgRelativeSrc)) {
          $imgRelativeSrc = utf8_decode($imgRelativeSrc);
        }
        if(file_exists($imgRelativeSrc)) {
          $type = exif_imagetype($imgRelativeSrc);
          $typeValid = 1;

          switch($type){
            case 1:
              $source = imagecreatefromgif($imgRelativeSrc);
              break;
            case 2:
              $source = imagecreatefromjpeg($imgRelativeSrc);
              break;
            case 3:
              $source = imagecreatefrompng($imgRelativeSrc);
              imagesavealpha($nouvelleImage, true);
              imagealphablending($nouvelleImage, false);
              $transparent = imagecolorallocatealpha($nouvelleImage, 0, 0, 0, 127);
              imagefill($nouvelleImage, 0, 0, $transparent);
              break;
            default:
              // invalide
              $typeValid = 0;
              break;
          }

          if($typeValid==1){

          if ($extension=='gif') {

            return $attImage[0];

            } else {

              imagecopyresampled($nouvelleImage, $source, -$decalageX, -$decalageY, 0, 0, $largeurImgSmall, $hauteurImgSmall, $largeurImgBig, $hauteurImgBig);

              if ($extension=='png') {
                imagepng($nouvelleImage,$abspath.'/images/'.$filenameext);
              } else {
                if ($avif==true) {
                  if (function_exists('imageavif')) {
                    imageavif($nouvelleImage,$abspath.'/images/'.$filenameext,$qual,1);
                  } else {
                    if (WP_DEBUG) {
                      // throw new customException('test custom error');
                      trigger_error("(wpbtw) AVIF not supported by GD lib before PHP8.1", E_USER_WARNING);
                    }
                    return false;
                  }
                } else {
                  imagejpeg($nouvelleImage,$abspath.'/images/'.$filenameext,$qual);
                  // imagewebp($nouvelleImage,$abspath.'/images/'.$filenameext);
                  // copyMeta($imgRelativeSrc, $imgpath.$filenameext);
                }
              }
              // return $domain.'/images/'.$filenameext;
              return optimizeImgObj($filename,$ext,$optimize);

            }

          } else {
            return "Format d'image invalide";
          }
        } else {
          return "Erreur : aucun fichier trouvé.";
        }

      } else {
        // return $domain.'/images/'.utf8_decode($filenameext);
        return optimizeImgObj(utf8_decode($filename),$ext,$optimize);
      }
    } else {
      // return $domain.'/images/'.$filenameext;
      return optimizeImgObj($filename,$ext,$optimize);
    }

  }
}

function optimizeImgObj($file='optimizeImgObj empty $file',$ext='optimizeImgObj empty $ext',$optimize=true) {
  $abspath = basepath();
  $domain = baseurl( get_bloginfo('url') );

  if (file_exists($abspath.'/images/'.$file.'-o'.$ext)) {
    return $domain.'/images/'.$file.'-o'.$ext;

  } else {

    if ($optimize && $ext=='.jpg') {
  
      if (!defined('TINIFYKEY')) {
        return $domain.'/images/'.$file.$ext;;
      }

      try {
        $source = \Tinify\fromFile($abspath.'/images/'.$file.$ext);
        // $copyrighted = $source->preserve("copyright", "creation");
        // $copyrighted->toFile($fileopt);
        // $copyrighted->toFile($abspath.'/images/'.$file.'-o'.$ext);
        $source->toFile($abspath.'/images/'.$file.'-o'.$ext);
        // copyMeta($abspath.'/images/'.$file.$ext, $abspath.'/images/'.$file.'-o'.$ext);

        $fileopt = $domain.'/images/'.$file.'-o'.$ext;
        return $fileopt;

      } catch(Exception $e) {

        return $domain.'/images/'.$file.$ext;

      }

    } else {
        return $domain.'/images/'.$file.$ext;
    }

  }

}

function picsum($width,$height) {
  if (!empty($width)) {
  $url = 'https://picsum.photos/'.$width;
    if (!empty($height)) {
      $url .= '/'.$height;
    }
    return $url;
  } else {
    return false;
  }
}

function copyMeta($inputPath, $outputPath) {
  // pr($inputPath);
  // pr(exif_read_data($inputPath));
  // exit;
  // pr($inputPath);
  // pr($outputPath);

  // $image = Image::fromFile($inputPath);
  // $headline = $image->getXmp()->getHeadline();
  // pr($headline);

/*
  $inputPel = new \lsolesen\pel\PelJpeg($inputPath);
  $outputPel = new \lsolesen\pel\PelJpeg($outputPath);
  if ($exif = $inputPel->getExif()) {
      $outputPel->setExif($exif);
  }
  if ($icc = $inputPel->getIcc()) {
      $outputPel->setIcc($icc);
  }
  $outputPel->saveFile($outputPath);
*/

  // $iptc_from = new Iptc($inputPath);
  // pr(Iptc::KEYWORDS);
  // $keywords = $iptc_from->fetchAll(Iptc::KEYWORDS);
  // $name = $iptc_from->fetchAll(Iptc::OBJECT_NAME);

  // pr(Iptc::KEYWORDS);
  // pr($keywords);
  // pr(Iptc::OBJECT_NAME);
  // pr($name);

  // $iptc_to = new Iptc($outputPath);
  // $iptc_to->set(Iptc::KEYWORDS, $keywords);
  // $iptc_to->set(Iptc::OBJECT_NAME,  $name);
  // $name = $iptc_to->fetchAll(Iptc::OBJECT_NAME);
  // pr($name);

  // try {
  //   $iptc_to->set(Iptc::OBJECT_NAME, 'tryiroyiuroyirtu');
  // } catch(Exception $e) {
  //   echo '--e--';
  //   pr($e);
  // }

  // $iptc_to->write();

  // exit;
  // echo "\nIptc->fetchAll: ";print_r($iptc->fetchAll(Iptc::KEYWORDS)) . "\n";
  // echo "\nIptc->dump: "; print_r($iptc->dump()) . "\n";

  // $iptc = new Iptc('logo_php.jpg');
  // $iptc->set(Iptc::KEYWORDS,array(
  //     'keyword1','This is a test with special characters ö, ä, ü'
  // ));
  // $iptc->set(Iptc::CITY, "São Paulo");
  // $iptc->write();

}






class customException extends Exception {
  public function errorMessage() {
    //error message
    $errorMsg = 'Error on line '.$this->getLine().' in '.$this->getFile()
    .': <b>'.$this->getMessage().'</b> is not a valid E-Mail address';
    return $errorMsg;
  }
}