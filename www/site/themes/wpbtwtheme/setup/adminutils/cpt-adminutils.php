<?php


function register_cpt_adminutils() {

  $labels = array(
    'name' => _x( 'Pages Admin', 'adminutils' ),
    'singular_name' => _x( 'Outil Admin', 'adminutils' ),
    'add_new' => _x( 'Ajouter', 'adminutils' ),
    'add_new_item' => _x( 'Ajouter', 'adminutils' ),
    'edit_item' => _x( 'Editer', 'adminutils' ),
    'new_item' => _x( 'Nouveau', 'adminutils' ),
    'view_item' => _x( 'Voir', 'adminutils' ),
    'search_items' => _x( 'Rechercher', 'adminutils' ),
    'not_found' => _x( 'Aucun', 'adminutils' ),
    'not_found_in_trash' => _x( 'Aucune', 'adminutils' ),
    'parent_item_colon' => _x( 'Parent :', 'adminutils' ),
    'menu_name' => _x( 'Pages Admin', 'adminutils' ),
  );

  $args = array(
    'labels' => $labels,
    'hierarchical' => false,
    'supports' => array( 'title', 'editor', 'thumbnail' ),
    //'taxonomies' => array( 'category', 'post_tag' ),
    'public' => true,
    'show_ui' => true,
    'show_in_menu' => true,
    'menu_position' => 5,
    'menu_icon' => 'dashicons-info',
    // Voir la liste => http://melchoyce.github.io/dashicons/
    'show_in_nav_menus' => true,
    'publicly_queryable' => true,
    'exclude_from_search' => true,
    'has_archive' => true,
    'query_var' => true,
    'can_export' => true,
    // 'rewrite' => true,
    'capability_type' => 'post',
    'rewrite' => array( 'slug' => ('') ),
  );

  register_post_type( 'adminutils', $args );
}
add_action( 'init', 'register_cpt_adminutils' );



/**
 * Remove the slug from published post permalinks.
 */

function custom_remove_cpt_slug( $post_link, $post, $leavename ) {
  if ( $post->post_type != 'adminutils' || $post->post_status != 'publish' ) {
    return $post_link;
  }
  $post_link = str_replace( '/' . $post->post_type . '/', '/', $post_link );
  return $post_link;
}
add_filter( 'post_type_link', 'custom_remove_cpt_slug', 10, 3 );



/**
 * Some hackery to have WordPress match postname to any of our public post types
 * All of our public post types can have /post-name/ as the slug, so they better be unique across all posts
 * Typically core only accounts for posts and pages where the slug is /post-name/
 */
/*
function custom_parse_request_tricksy( $query ) {

if (is_user_logged_in()) {
  $uid = get_current_user_id();
  if ($uid===1) {
    echo 'aaaaaaaa';

    if ($query->query['post_type']=='adminutils') {

      if ( ! $query->is_main_query() || 2 != count( $query->query ) || ! isset( $query->query['page'] ) ) {
        echo '1';
      }
      if ( ! empty( $query->query['name'] ) ) {
        echo '2';
      }
        echo '3';
      // pr($query);
      exit;
    }

  }
}

  if (!is_admin()) {

    if ( ! $query->is_main_query() || 2 != count( $query->query ) || ! isset( $query->query['page'] ) ) {
      return;
    }

    if ( ! empty( $query->query['name'] ) ) {
      $query->set( 'post_type', array( 'post', 'adminutils', 'page' ) );
    }

  }

  return $query;

}
// add_action( 'pre_get_posts', 'custom_parse_request_tricksy' );
*/
