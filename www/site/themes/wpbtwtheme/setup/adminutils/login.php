<?php
$msg = '';

if (is_user_logged_in()) {
  wp_redirect( admin_url() );
  exit;
}
/*
if (!empty($_GET['registration'])) {
  $registration = $_GET["registration"];
}
if (!empty($_GET['redirect'])) {
  $redirect = $_GET["redirect"];
}
*/

// function wpdocs_custom_login() {
  // echo 'aaaa';exit;
    // $creds = array(
    //     'user_login'    => 'example',
    //     'user_password' => 'plaintextpw',
    //     'remember'      => true
    // );

    // $user = wp_signon( $creds, false );

    // if ( is_wp_error( $user ) ) {
    //     echo $user->get_error_message();
    // }
// }

// Run before the headers and cookies are sent.
// add_action( 'after_setup_theme', 'wpdocs_custom_login' );



if (isset($_POST['log']) && isset($_POST['pwd'])) {
  $valid = true;
  $errors = array();

  if (empty($_POST['log'])) {
    $valid = false;
    $errors[] = "Login vide";
  }

  if (empty($_POST['pwd'])) {
    $valid = false;
    $errors[] = "Mot de passe vide";
  }

  if (isset($_POST['rememberme'])) {
    $rememberme = true;
  } else {
    $rememberme = false;
  }
  if ($valid) {
    $creds = array(
        'user_login'    => esc_attr($_POST['log']),
        'user_password' => $_POST['pwd'],
        'remember'      => $rememberme
    );
    $user = wp_signon( $creds, false );

    // pr($user);
    // exit;

    if (!is_wp_error($user)) {
      wp_set_current_user($user->ID);
      wp_set_auth_cookie($user->ID);
      do_action( 'wp_login', $user->user_login, $user );
      // pr($user->ID);
      // pr($user->user_login);
      // pr($user);
      // if ( in_array( 'administrator', $user->roles ) ) {
        // wp_set_current_user($user->ID);
        // pr(admin_url());exit;
// pr(is_user_logged_in());
      if (is_user_logged_in()) {
        wp_redirect( admin_url() );
        exit;
      }
    } else {
      $msg = $user->get_error_message();
    }
  }
}
/*
if (!empty($action)) {
  if ($action == "logout" && is_user_logged_in()) {
    wp_clear_auth_cookie();
    if(!empty($redirect)) {
      wp_safe_redirect($redirect);
    } else {
      wp_safe_redirect( get_bloginfo('url').'/?action=logout');
    }
  } else if ($action == 'lostpassword' && !is_user_logged_in() ) {
    $redirect_to = get_bloginfo('url')."/reset";
    wp_redirect($redirect_to);
    exit();
  }
}
*/

get_clean_header();

// $nonce = wp_create_nonce('customlogin');
// $nonce = md5(time().'s4lTy');
// update_option('login_nonce',$nonce);

if (!empty($errors)) {
  // pr($errors);
}
?>

<h1>LOGIN</h1>

<?php
echo $msg;
?>

    <form action="<?php echo get_permalink(); ?>" method="POST" id="wpbtw_loginform" name="wpbtw_loginform">

      <div>
        <input id="my_email" maxlength="80" name="log" size="20" type="text" placeholder="Login" value="<?php if(!empty($_POST['log']) && !empty($_POST['pwd'])){ echo $_POST['log']; } ?>" />
      </div>

      <div>
        <input  id="my_pwd" maxlength="40" name="pwd" size="20" type="password" placeholder="Password" value="<?php if(!empty($_POST['log']) && !empty($_POST['pwd'])){ echo $_POST['pwd']; } ?>" />
      </div>

      <div>
        <input name="rememberme" id="rememberme" type="checkbox" checked="<?php if(!empty($_POST['rememberme'])){ echo $_POST['rememberme']; }else{ echo 'checked'; } ?>" value="forever" /><label id="rememberme-label" for="rememberme"><?php _e("Se souvenir de moi"); ?></label><br>
        <!-- <a href="<?php bloginfo('url'); ?>/admin?action=lostpassword" title="Lost Password"><?php _e("Mot de passe oublié ?"); ?></a> -->
      </div>

      <div>
        <input class="pleaselogme" type="submit" name="submit" value="Valider">
      </div>

      <div>
        <a href="<?php echo get_permalink(6) ?>">J'ai perdu mon mot de passe</a>
      </div>

    </form>

<?php

/*
  global $user_login, $wpdb, $user_ID, $current_user;
  $action = ""; $login = ""; $checkemail = ""; $registration = ""; $redirect = "";
  if (!empty($_GET['action'])) { $action = $_GET["action"]; }
  if (!empty($_GET['login'])) { $login = $_GET["login"]; }
  if (!empty($_GET['checkemail'])) { $checkemail = $_GET["checkemail"]; }
  if (!empty($_GET['registration'])) { $registration = $_GET["registration"]; }
  if (!empty($_GET['redirect'])) { $redirect = $_GET["redirect"]; }
  if (!empty($action)) {
    if ($action == "logout" && is_user_logged_in()) {
      wp_clear_auth_cookie();
      if(!empty($redirect)) {
        wp_safe_redirect($redirect);
      } else {
        wp_safe_redirect( get_bloginfo('url').'/?action=logout');
      }
    } else if ($action == 'lostpassword' && !is_user_logged_in() ) {
      $redirect_to = get_bloginfo('url')."/reset";
      wp_redirect($redirect_to);
      exit();
    }
  }

  get_clean_header();

  if ( !empty($_POST['log']) && !empty($_POST['pwd']) ) { ?>
    <style>body{display:none}</style>
  <?php }

  $url_base = "http://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
  $url = preg_replace('/\?.* /', '', $url_base);

  if (!$user_ID) {
    if(empty($action) || $action == 'logout' || $action == 'logout?login=failed' || $action == 'reset_success'){
?>
  <div class="login-box">
    <?php if ($login == 'failed' || $action == 'logout?login=failed'){ ?>
      <div class="infos-box alert"> <?php _e("Erreur : Merci de vérifier votre identifiant et votre mot de passe."); ?> </div>
    <?php } if ($action == 'logout') { ?>
      <div class="infos-box"> <?php _e("Vous êtes désormais déconnecté(e)."); ?> </div>
    <?php } if ($action == 'reset_success') { ?>
      <div class="infos-box"> <?php _e("Votre nouveau mot de passe est arrivé dans votre boite mail, vous pouvez dès à présent l'utiliser."); ?> </div>
    <?php } if ($checkemail == 'confirm') { ?>
      <div class="infos-box"> <?php _e("Un email vient d'être envoyé à votre adresse."); ?> </div>
    <?php } if ($registration == 'disabled') { ?>
      <div class="infos-box alert"> <?php _e("Les nouvelles inscriptions ne sont pas autorisées pour l’instant."); ?> </div>
    <?php } ?>
    <form action="<?php if(!empty($_POST['log']) && !empty($_POST['pwd'])) { echo wp_login_url( get_permalink()).'?key=32658745962'; } ?>" method="POST" id="loginForm" name="loginForm" onsubmit="return validate_connect()" >
      <input  id="my_email" maxlength="80" name="log" size="20" type="text" placeholder="Login" value="<?php if(!empty($_POST['log']) && !empty($_POST['pwd'])){ echo $_POST['log']; } ?>" />
      <input  id="my_pwd" maxlength="40" name="pwd" size="20" type="password" placeholder="Password" value="<?php if(!empty($_POST['log']) && !empty($_POST['pwd'])){ echo $_POST['pwd']; } ?>" />
      <input name="rememberme" id="rememberme" type="checkbox" checked="<?php if(!empty($_POST['rememberme'])){ echo $_POST['rememberme']; }else{ echo 'checked'; } ?>" value="forever" /><label id="rememberme-label" for="rememberme"><?php _e("Se souvenir de moi"); ?></label><br>
      <a href="<?php bloginfo('url'); ?>/admin?action=lostpassword" title="Lost Password"><?php _e("Mot de passe oublié ?"); ?></a><br>
      <?php if(!empty($_POST['log']) && !empty($_POST['pwd'])){ ?>
        <script>document.getElementById("loginForm").submit();</script>
      <?php }else{ ?>
        <input type="submit" name="submit" value="Valider">
      <?php } ?>
    </form>
    <script>
      function validate_connect(){
        var log=document.forms["loginForm"]["log"].value;
        var pwd=document.forms["loginForm"]["pwd"].value;
        if (log==null || log==""){ alert("Merci de renseigner votre login"); return false; }
        if (pwd==null || pwd==""){ alert("Merci de renseigner votre mot de passe"); return false; }
      }
    </script>
    <div class="clear-both"></div>
  </div>
<?php } ?>
<?php } else { ?>
  <div class="login-box" id="lost-pswd">
    <div class="infos-box normal"> <?php _e("Vous êtes déjà connecté"); ?> </div>
    <a class="submit first" href="<?php echo get_bloginfo('url').'/admin?action=logout' ?>">Déconnexion</a>
    <?php
      if ( is_array( $current_user->roles ) ) {
        if ( in_array( 'administrator', $current_user->roles ) ){
    ?>
      <a class="submit" href="<?php echo get_bloginfo('url').'/wp-admin'; ?>">Backoffice</a>
    <?php }else{ ?>
      <a class="submit" href="<?php echo get_bloginfo('url'); ?>">Accueil</a>
    <?php }} ?>
    <div class="clear-both"></div>
  </div>
<?php } ?>

<?php get_footer(); ?>
*/



get_clean_footer();
