<?php 

$modemaintenance = get_field('activer_le_mode_maintenance',7);
if (empty($modemaintenance)) {
  wp_safe_redirect(get_home_url());
  exit;
}

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta charset="<?php bloginfo('charset'); ?>">
  <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
  <meta http-equiv="content-language" content="<?php echo get_bloginfo('language'); ?>" />
  <meta name='viewport' content="width=device-width, initial-scale=1.0, user-scalable=no">
  <meta name="apple-mobile-web-app-capable" content="yes" />

  <title><?php wp_title() ?></title>
  <meta name="description" content="<?php the_title() ?>">

  <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css">

  <link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_home_url() ?>/fav/apple-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_home_url() ?>/fav/apple-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_home_url() ?>/fav/apple-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_home_url() ?>/fav/apple-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_home_url() ?>/fav/apple-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_home_url() ?>/fav/apple-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_home_url() ?>/fav/apple-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_home_url() ?>/fav/apple-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_home_url() ?>/fav/apple-icon-180x180.png">
  <link rel="icon" type="image/png" sizes="192x192" href="<?php echo get_home_url() ?>/fav/android-icon-192x192.png">
  <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_home_url() ?>/fav/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="96x96" href="<?php echo get_home_url() ?>/fav/favicon-96x96.png">
  <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_home_url() ?>/fav/favicon-16x16.png">
  <link rel="manifest" href="<?php echo get_home_url() ?>/fav/manifest.json" crossorigin="use-credentials">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="<?php echo get_home_url() ?>/fav/ms-icon-144x144.png">
  <meta name="theme-color" content="#ffffff">
</head>
<body>

  <div class="maintenancemode">
  <?php 
  /*the_title()*/ 
  $img = getImageObj($post->ID,2000,0,75,false,true);
  if (!empty($img)) {
    echo '<img src="'.$img->src.'" alt="'.$img->post_title.'">';
  } else {
    echo '
    <div>
      <h1>'.$post->post_title.'</h1>
      '.wpautop($post->post_content).'
    </div>';
  }
  ?>
  </div>

</body></html><?php 
