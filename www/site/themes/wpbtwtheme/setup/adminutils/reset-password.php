<?php
if (isset($_POST['required']) && !empty($_POST['required'])) {
	exit;
}

global $wpdb, $user_ID;

add_filter("lostpassword_url", "custom_login_lostpassword_url");
function custom_login_lostpassword_url() {
  return get_permalink(6);
}

$msg = '';


$asknewpwd = false;
if (!empty($_GET['u'])) {

	if (empty($_GET['sk'])) {
		exit;
	}

	$the_user = get_user_by( 'id', intval($_GET['u']) );
	if (!empty($the_user)) {

		$secretcode = get_user_meta($the_user->ID,'secretcode',true);
		if ($secretcode!=$_GET['sk'] || empty($_GET['sk'])) {
			exit;
		}

		$datesecretcode = get_user_meta($the_user->ID,'datesecretcode',true);
		if (!empty($datesecretcode) && !empty($secretcode)) {
			$currentdate = time();
			if (($currentdate-$datesecretcode)<3600) {
				$asknewpwd = true;
			} else {
				$msg .= "<p>Ce lien n'est plus valable.</p>";
			}
		} else {
			$msg .= "<p>Ce lien n'est plus valable.</p>";
		}

	} else  {
		$msg .= "<p>Le compte n'a pas été trouvé.</p>";
	}

}


if (!empty($_POST['action']) && $_POST['action']=='set_new_password') {
	if (!empty($_POST['user_newpwd'])) {
		$uid = intval($_POST['unewpws']);
		if ( empty($uid) || !wp_verify_nonce($_POST['newpwd_nonce'], "newpwd_nonce") ) {
		  exit;
	  }
		$secretcode = get_user_meta($uid,'secretcode',true);
		$datesecretcode = get_user_meta($the_user->ID,'datesecretcode',true);
		$currentdate = time();
	  if (!empty($secretcode) && $_POST['sknewpws']==$secretcode && ($currentdate-$datesecretcode)<3600) {
	  	wp_set_password( $_POST['user_newpwd'], $uid );
			$msg .= "<p>Votre mot de passe a bien été changé.</p>";
			$msg .= '<p><a href="'.get_permalink(5).'" title="Connexion">Me connecter</a></p>';
			delete_user_meta($uid,'secretcode');
			delete_user_meta($uid,'datesecretcode');
	  }
	} else {
		$msg .= "<p>Impossible d'utiliser un mot de passe vide.</p>";
	}
}


get_clean_header();
?>
<div class="resetpassword">
	<div class="mainwrapper">

		<h1><?php echo $post->post_title ?></h1>

		<?php if (!empty($msg)) { 
			echo '<div class="txycenter messages">';
			echo $msg;
			echo '</div>';
		} ?>

		<?php 
		if ($asknewpwd) {
		?>
			<div class="txycenter">
		    <form action="<?php echo wp_lostpassword_url(); ?>" method="post">

		    	<div>
	          <label for="user_newpwd">Choisissez un nouveau mot de passe :
	          <input type="password" name="user_newpwd" id="user_newpwd">
	        </div>

		    	<div>
	          <input type="submit" name="submit" class="lostpassword-button" value="Reset Password">
						<input type="hidden" name="newpwd_nonce" value="<?php echo wp_create_nonce("newpwd_nonce"); ?>" />
						<input type="hidden" name="sknewpws" value="<?php echo $secretcode ?>">
						<input type="hidden" name="unewpws" value="<?php echo intval($_GET['u']) ?>">
						<input type="hidden" name="action" value="set_new_password">
	        </div>

		    </form>
			</div>
		<?php
		}
		?>

		<?php 

	if(!empty($_POST['user_email'])) {

		if ( !wp_verify_nonce($_POST['pwd_nonce'], "pwd_nonce") ) {
		  exit;
	  }

		$user_email = $_POST['user_email'];


			$user_data = get_user_by('email', $user_email);

			// if(empty($user_data) || !empty($user_data->caps['administrator'])) {
			if(empty($user_data)) {
				sleep(5);
				echo "<div class='rpwd_infobox txycenter'>
					<p>Ce compte n'existe pas ou n'a pas été trouvé.</p>
				</div>";

			} else {

				$secret = md5(json_encode($user_data).'SaltyButter'.time());
				$lien = wp_lostpassword_url().'?u='.$user_data->ID.'&sk='.$secret;
				update_user_meta($user_data->ID,'secretcode',$secret);
				update_user_meta($user_data->ID,'datesecretcode',time());
				$headers = array( 'Content-Type: text/html; charset=UTF-8' );
				// $headers = array('Content-Type: text/html; charset=UTF-8','From: My Site Name <support@example.com>');
				$message = '
					<p>Pour définir un nouveau mot de passe, cliquez sur le lien suivant :</p>
					<p><a href="'.$lien.'">'.$lien.'</a></p>
					<p>Ce lien s\'auto-détruira dans moins d\'une heure.</p>
				';
				$mail = wp_mail($user_email, 'Renouvellement de mot de passe', $message, $headers, array( '' ));
				// pr($message);
				sleep(5);
				if (!$mail) {
					$msg .= "<p>Erreur : Impossible d'envoyer l'email.</p>";
				}

			}

	}




		if ($user_ID) {

			echo '
			<div class="txycenter">
				<p>Vous êtes connecté(e).</p>
				<p><a href="'.wp_logout_url().'" title="Déconnexion">[ Me déconnecter ]</a></p>
			</div>
			';

		} else {

			if (!$asknewpwd) {
			?>
				<div class="txycenter">
			    <form action="<?php echo wp_lostpassword_url(); ?>" method="post">

			    	<div>
		          <label for="user_email">Votre adresse email :
		          <input type="email" name="user_email" id="user_email">
		        </div>

			    	<div>
		          <input type="submit" name="submit" class="lostpassword-button" value="Reset Password">
							<input type="hidden" name="pwd_nonce" value="<?php echo wp_create_nonce("pwd_nonce"); ?>" />
							<input type="hidden" name="required" value="" />
		        </div>

			    </form>
				</div>
			<?php
			}
		}
		?>

	</div>
</div>
<?php 
get_clean_footer();

