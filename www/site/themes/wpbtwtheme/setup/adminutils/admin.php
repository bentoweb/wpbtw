<?php
/**
 * Custom Login System
 * Goodbye wp- :)
 *
 */


// Remove redirection from /admin to /wp-admin (to free the /admin url)
remove_action( 'template_redirect', 'wp_redirect_admin_locations', 1000 );


function custom_login(){
  global $pagenow;
  $slug = basename(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));

  if ($slug=='wp-admin' && !is_user_logged_in()) {
    wp_clear_auth_cookie();
    status_header( 404 );
    nocache_headers();
    include( get_query_template( '404' ) );
    die();
  }

  if ( $pagenow == 'wp-login.php'  ) {

    if (is_user_logged_in()) {
      wp_safe_redirect( admin_url() );

    } else {
      status_header( 404 );
      nocache_headers();
      include( get_query_template( '404' ) );
      die();

    }

  }


}
add_action('init','custom_login');


// add_action('wp_login', 'wploginhook', 10, 3);
// function wploginhook($u) {
  // echo 'oo';
  // pr($u);
  // pr(is_user_logged_in());
  // exit;
// }


// function redirectall404() {
//   if (is_404()) {
//     $page = basename($_SERVER['REQUEST_URI']);
//     if ($page!='404') {
//       wp_safe_redirect( get_bloginfo('url').'/404' );
//     }
//   }
// }
// add_action('get_header','redirectall404');


/**
 * Disable admin (re)login popup
 */
remove_action( 'admin_enqueue_scripts', 'wp_auth_check_load' );

/**
 * Hide page reset and admin on page list
 * Look backoffice.php
 */

/**
 * Add button for edit login and reset page to option sub-menu
 */
/*
add_action('admin_menu', 'register_submenu_login_reset');
function register_submenu_login_reset() {
  add_submenu_page( 'options-general.php', 'Login', 'Login', 'administrator', 'post.php?post='.get_id_by_name("Admin").'&action=edit&message=1' );
  add_submenu_page( 'options-general.php', 'Reset password', 'Reset password', 'administrator', 'post.php?post='.get_id_by_name("Reset").'&action=edit&message=1' );
}
*/


// add_filter( 'logout_url', 'wpse_58453_logout_url' );
// function wpse_58453_logout_url($default) {
//     // set your URL here
//     return get_bloginfo('url');
// }



add_action('wp_logout','auto_redirect_after_logout');
function auto_redirect_after_logout(){
  wp_redirect( home_url() );
  exit();
}