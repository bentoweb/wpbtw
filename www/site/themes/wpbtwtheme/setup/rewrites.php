<?php
/**
 * URL rewriting
 *
 */

function roots_flush_rewrites() {
  global $wp_rewrite;
  $wp_rewrite->flush_rules();
}
add_action('admin_init', 'roots_flush_rewrites');

// Stop WordPress from modifying .htaccess permalink rules
if (!WP_DEBUG) {
  add_filter('flush_rewrite_rules_hard','__return_false');
}

/*
function roots_add_rewrites($content) {
  global $wp_rewrite;

  //---- PARENT
  $themePath = get_template_directory();
  $themePath = explode('/',$themePath);
  $themePath = end($themePath);
  $roots_new_non_wp_rules = array(
    'parent/assets/(.*)'      => 'site/themes/'. $themePath . '/assets/$1',
    'ajax/(.*)'      => 'wp-admin/admin-ajax.php',
    'plugins/(.*)'  => 'site/plugins/$1'
  );
  $wp_rewrite->non_wp_rules += $roots_new_non_wp_rules;

  //---- CHILD
  $ssd = explode('/themes/', get_stylesheet_directory());
  $theme_name = next($ssd);
  $roots_new_non_wp_rules = array(
    'assets/(.*)'      => 'site/themes/'. $theme_name . '/assets/$1',
  );
  $wp_rewrite->non_wp_rules += $roots_new_non_wp_rules;
}
add_action('generate_rewrite_rules', 'roots_add_rewrites');
*/

function btw_custom_theme_rewrite_rules() {
  //---- PARENT
  $themePath = get_template_directory();
  $themePath = explode('/',$themePath);
  $themePath = end($themePath);
  add_rewrite_rule('parent/assets/(.*)', 'site/themes/'. $themePath . '/assets/$1', 'top');
  add_rewrite_rule('ajax/(.*)', 'wp-admin/admin-ajax.php', 'top');
  add_rewrite_rule('plugins/(.*)', 'site/plugins/$1', 'top');

  //---- CHILD
  $ssd = explode('/themes/', get_stylesheet_directory());
  $theme_name = next($ssd);
  add_rewrite_rule('assets/(.*)', 'site/themes/'. $theme_name . '/assets/$1', 'top');

  //---- ADMINUTILS
  $adminutilsposts = get_posts(array(
    'numberposts' => -1,
    'post_type'   => 'adminutils'
  ));
  foreach ($adminutilsposts as $apost) {
    if ($apost->post_name!='404') {
      add_rewrite_rule($apost->post_name, '?adminutils='.$apost->post_name, 'top');
    }
  }
}
add_action('init', 'btw_custom_theme_rewrite_rules');



function roots_clean_assets($content) {
  $ssd = explode('/themes/', get_stylesheet_directory());
  $theme_name = next($ssd);
  $current_path = '/site/themes/' . $theme_name;
  $new_path = '';
  $content = str_replace($current_path, $new_path, $content);
  return $content;
}

function roots_clean_plugins($content) {
  $current_path = '/site/plugins';
  $new_path = '/plugins';
  $content = str_replace($current_path, $new_path, $content);
  return $content;
}

if (!is_admin()) {
  add_filter('bloginfo', 'roots_clean_assets');
  add_filter('stylesheet_directory_uri', 'roots_clean_assets');
  add_filter('template_directory_uri', 'roots_clean_assets');
  add_filter('plugins_url', 'roots_clean_plugins');
  add_filter('script_loader_src', 'roots_clean_plugins');
  add_filter('style_loader_src', 'roots_clean_plugins');
}



