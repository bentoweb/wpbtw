<?php

//Initialisation
add_action('add_meta_boxes','meta_accueil_init');
function meta_accueil_init(){
    add_meta_box('accueil_base', 'Nom de la box', 'meta_accueil', 'page', 'normal', 'default');
}

function meta_accueil($post){
  global $wpdb;
  $titre_exemple = get_post_meta($post->ID,'_titre_exemple',true);
  $textarea_exemple = get_post_meta($post->ID,'_textarea_exemple',true); 
  $editeur_texte_exemple = html_entity_decode(get_post_meta($post->ID,'_editeur_texte_exemple',true));
    $args = array ( 'tinymce' => true, 'quicktags' => true, 'media_buttons' => false);
  ?>
    <table class="form-table" style="width:100%">
    
      <tr style="width:100%">
        <th style="width:30%"> <label>Titre : </label></th>
        <td style="width:70%"><input type="text" style="width:100%;" name="titre_exemple" value='<?php echo $titre_exemple; ?>' /></td>
      </tr>
      
      <tr style="width:100%">
        <th style="width:30%"> <label>Textarea : </label></th>
        <td style="width:70%"><textarea style="width:100%; height:100px;" name="textarea_exemple"><?php echo $textarea_exemple; ?></textarea></td>
      </tr>
      
      <tr style="width:100%">
        <th style="width:30%"> <label>Éditeur de texte : </label></th>
        <td style="width:70%"><?php wp_editor( $editeur_texte_exemple, '_editeur_texte_exemple', $args ); ?></td>
      </tr>
      
    </table>
<?php }

// Sauvegarde
add_action('save_post','save_meta_accueil');
function save_meta_accueil($post_id){

  // meta_accueil_developpez_competences
  if(isset($_POST['titre_exemple'])){ 
    update_post_meta($post_id, '_titre_exemple', esc_html($_POST['titre_exemple'])); 
  }
  if(isset($_POST['textarea_exemple'])){ 
    update_post_meta($post_id, '_textarea_exemple', esc_html($_POST['textarea_exemple'])); 
  }
  if ( isset ( $_POST['_editeur_texte_exemple'] ) ) {
    update_post_meta( $post_id, '_editeur_texte_exemple', esc_html($_POST['_editeur_texte_exemple']));
  }
}
