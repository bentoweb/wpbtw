<?php

/* //////////////////////////////////////////////////////////////////////////////////////////////////////////// */
/* ///////////////////////////////////////////////// NOTICE /////////////////////////////////////////////////// */
/* //////////////////////////////////////////////////////////////////////////////////////////////////////////// */
/*

    1 / Déclarer la metabox dans le function.php
          require_once(TEMPLATEPATH.'/cmb/box-slideshow.php');
    2 / Déclarer la taille des images du slideshow dans le fonction.php
          if ( function_exists( 'add_image_size' ) ) {
            add_image_size( 'slider', 870, 350, true );
          }
    4 / Ligne 46 faire attention au nom du template!
    5 / Si vous voulez activer le slider partout, utiliser le code ligne 51 et effacer entre l42 et l49
    3 / Ajouter les images dans un template :
          <?php
            $img_link = get_post_meta($post->ID,'_descr_img',false);
            $img_titre = get_post_meta($post->ID,'_descr_img_titre',false);
            $img_legend = get_post_meta($post->ID,'_descr_img_legend',false);
            $img_alt = get_post_meta($post->ID,'_descr_img_alt',false);

            $img_id = get_post_meta($post->ID,'_descr_id',false);
            foreach($img_id as $k => $v){
              echo wp_get_attachment_image($v, 'slider', false);
            }
          ?>

*/
/* //////////////////////////////////////////////////////////////////////////////////////////////////////////// */
/* //////////////////////////////////////////////// FIN /////////////////////////////////////////////////////// */
/* //////////////////////////////////////////////////////////////////////////////////////////////////////////// */

function scripts_slideshow() {
  wp_enqueue_script('media-upload');
  wp_enqueue_media();
}

add_action('admin_print_scripts', 'scripts_slideshow');

//Initialisation
$post_id; $template_file;
if(!empty($_GET['post'])){
  $post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
  $template_file = get_post_meta($post_id,'_wp_page_template',TRUE);
  if ($template_file == 'templates/accueil.php') {
    add_action('add_meta_boxes','meta_slideshow_init');
  }
}

//add_action('add_meta_boxes','meta_slideshow_init');

function meta_slideshow_init(){
  add_meta_box('slidshow', 'Slideshow', 'meta_slideshow', 'page', 'normal', 'default');
}

//fonction alternative à get_post_meta
/*
function get_post_meta_ordered($id,$meta_key){
  global $wpdb;
  $output = array();
  $sql = "SELECT m.meta_value FROM ".$wpdb->postmeta." m where m.meta_key = '".$meta_key."' and m.post_id = '".$id."' order by m.meta_id";
  $results = $wpdb->get_results( $sql );
  foreach( $results as $result ){
    $output[] = $result->meta_value;
  }
  return array_filter($output);
}
*/
// Fonction de construction de la metabox
function meta_slideshow($post){
  global $wpdb;

  $img_id = get_post_meta_ordered($post->ID,'_descr_id');
  $img = get_post_meta_ordered($post->ID,'_descr_img');
  $img_titre = get_post_meta_ordered($post->ID,'_descr_img_titre');
  $img_legend = get_post_meta_ordered($post->ID,'_descr_img_legend');
  $img_alt = get_post_meta_ordered($post->ID,'_descr_img_alt');

  //boucle
  echo '<div id="all_img">';
  if(count( $img )>0):
    foreach($img as $k => $img_url){
      ?>
      <table  style="border-bottom:1px solid #ddd; margin-bottom:10px; padding-bottom:10px;" class="item-img">
        <tr>
          <th><a class="remove-img button-secondary hide-if-no-js" style="margin-right:10px" href="javascript:void(0);">-</a></th>
          <td style="width:10%"><label>Lien:</label></td>
          <td style="width:80%"><input readonly class="upload_image_input upload" style="width:95%" type="text" size="36" name="descr_img[]" value="<?php echo $img_url; ?>" /></td>
          <td><input class="upload_image_id upload" type="hidden" name="descr_id[]" value="<?php echo $img_id[$k]; ?>" /></td>
          <td><input class="upload_image_alt upload" type="hidden" name="descr_alt[]" value="<?php echo $img_alt[$k]; ?>" /></td>
          <td><input class="upload_image_button button-secondary" style="margin-right:10px" type="button" value="Upload Image" /><br></td>
        </tr>
        <tr>
          <th></th>
          <td style="width:10%"><label>Titre:</label></td>
          <td style="width:80%"><input class="upload_image_titre upload" style="width:95%" type="text" size="36" name="descr_titre[]" value="<?php echo $img_titre[$k]; ?>" /><br></td>
        </tr>
        <tr>
          <th></th>
          <td style="width:10%"><label>Légende:</label></td>
          <td style="width:80%"><input class="upload_image_legend upload" style="width:95%" type="text" size="36" name="descr_legend[]" value="<?php echo $img_legend[$k]; ?>" /></td>
        </tr>
      </table>
      <?php
    }
  endif; ?>
  <table  style="border-bottom:1px solid #ddd; margin-bottom:10px; padding-bottom:10px;" class="item-img">
    <tr>
      <th><a class="remove-img button-secondary hide-if-no-js" style="margin-right:10px" href="javascript:void(0);">-</a></th>
      <td style="width:10%"><label>Lien:</label></td>
      <td style="width:80%"><input readonly class="upload_image_input upload" style="width:95%" type="text" size="36" name="descr_img[]" value="" /></td>
      <td><input class="upload_image_id upload" type="hidden" name="descr_id[]" value="" /></td>
      <td><input class="upload_image_alt upload" type="hidden" name="descr_alt[]" value="" /></td>
      <td><input class="upload_image_button button-secondary" style="margin-right:10px" type="button" value="Upload Image" /><br></td>
    </tr>
    <tr>
      <th></th>
      <td style="width:10%"><label>Titre:</label></td>
      <td style="width:80%"><input class="upload_image_titre upload" style="width:95%" type="text" size="36" name="descr_titre[]" value="" /><br></td>
    </tr>
    <tr>
      <th></th>
      <td style="width:10%"><label>Légende:</label></td>
      <td style="width:80%"><input class="upload_image_legend upload" style="width:95%" type="text" size="36" name="descr_legend[]" value="" /></td>
    </tr>
  </table>
  <?php
  echo '</div>';
  ?>

  <!-- lien ajout -->
  <a id="add-img" class="button-primary hide-if-no-js" style="margin: 10px 10px 0 0; position: relative; display: inline-block;" href="javascript:void(0);">Add more</a>

  <script type="text/javascript">

    jQuery(document).ready(function($){

      var _custom_media = true,
          _orig_send_attachment = wp.media.editor.send.attachment;


      //init les btn upload
      function add_upload(){
         $('.upload_image_button').click(function(e) {
          var send_attachment_bkp = wp.media.editor.send.attachment;
          var button = $(this);
          var $current_table = button.parent().parent().parent();
          var input_id = $('.upload_image_id', $current_table)
          ,   input_url = $('.upload_image_input', $current_table)
          ,   input_alt = $('.upload_image_alt', $current_table)
          ,   input_titre = $('.upload_image_titre', $current_table)
          ,   input_legende = $('.upload_image_legend', $current_table);
          _custom_media = true;
          wp.media.editor.send.attachment = function(props, attachment){
              var img_url = attachment.url
              ,   img_id = attachment.id
              ,   img_alt = attachment.alt
              ,   img_titre = attachment.name
              ,   img_legende = attachment.caption;
            if ( _custom_media ) {
                $(input_id).val(img_id);
                $(input_url).val(img_url);
                $(input_alt).val(img_alt);
                $(input_titre).val(img_titre);
                $(input_legende).val(img_legende);
            } else {
              return _orig_send_attachment.apply( this, [props, attachment] );
            };
          }
          wp.media.editor.open(button);
          return false;
        });
      }
      // init le premier btn upload
      add_upload();

      // bloque le add media d'origine
      $('.add_media').on('click', function(){
        _custom_media = false;
      });

      /* /////////////////////////////////////////////// */
      /* /////// AJOUTER OU ENLEVER UNE LIGNE ////////// */
      /* /////////////////////////////////////////////// */
      function remove_chose(){
        jQuery('.remove-img').on('click',function(){
          jQuery(this).parent().parent().parent().parent().remove();
        });
      }
      remove_chose();

      jQuery('#add-img').on('click',function(){
        jQuery('.item-img:last').clone().appendTo('#all_img');
        jQuery('.item-img:last .upload_image_input').val('');
        jQuery('.item-img:last .upload_image_id').val('');
        jQuery('.item-img:last .upload_image_titre').val('');
        jQuery('.item-img:last .upload_image_legend').val('');
        jQuery('.item-img:last .upload_image_alt').val('');
        remove_chose();
        add_upload();
      });

    });

  </script>

  <?php }
  // Sauvegarde
  add_action('save_post','save_metabox_slideshow');
  function save_metabox_slideshow($post_id){
  if( ( !defined( 'DOING_AJAX' ) || !DOING_AJAX ) && isset( $_POST['descr_id'], $_POST['descr_img'], $_POST['descr_legend'], $_POST['descr_alt']) ){
    delete_post_meta($post_id, '_descr_id');
    delete_post_meta($post_id, '_descr_img');
    delete_post_meta($post_id, '_descr_img_titre');
    delete_post_meta($post_id, '_descr_img_legend');
    delete_post_meta($post_id, '_descr_img_alt');
    foreach($_POST['descr_id'] as $d){
      if(!empty($d))
        add_post_meta($post_id, '_descr_id', $d);
    }
    foreach($_POST['descr_img'] as $d){
      if(!empty($d))
        add_post_meta($post_id, '_descr_img', $d);
    }
    foreach($_POST['descr_titre'] as $d){
      if(!empty($d))
        add_post_meta($post_id, '_descr_img_titre', $d);
    }
    foreach($_POST['descr_legend'] as $d){
      if(!empty($d))
        add_post_meta($post_id, '_descr_img_legend', $d);
    }
   foreach($_POST['descr_alt'] as $d){
      if(!empty($d))
        add_post_meta($post_id, '_descr_img_alt', $d);
    }
  }
}
