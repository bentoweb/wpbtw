<?php
/**
 * Custom functions for btw WordPress
 *
 */

/*
  date_fr ($date,$format)
  cut_string ($string,$longueur=120)
  cutMyWords ($chaine,$limit_caracts)
  get_id_by_name ($page_name)
  excerpt ($limit)
  content ($limit)
  pr ($cible,$die=false)
  rw_remove_root ($url)
  getbimg(array)
  getImageObj($idPost,$imgW=0,$imgH=0,$qual=90,$default=false,$optimize=true)
  clearFolder ($folder)
  get_nav ($menu_name = "menu_header", $id_menu="")
  get_nav_ul ($menu_name="menu_header_fr", $id_menu=null, $urls=null)
  build_nav ($parent_id=0,$menu_items,$urls=null)
  alias ($chaine)
  lighten ($color_code,$percentage = 0)
  darken ($color_code,$percentage = 0)
  get_post_meta_ordered ($id,$meta_key)
  svg_icon ($id,$extraClass)
  assets()
  btw_pagination ($page,$maxpage,$prevnextbt,$minmaxbt,$baseurl)
*/

function isTimestamp($string) {
  try {
    new DateTime('@' . $string);
  } catch(Exception $e) {
    return false;
  }
  return true;
}

/**
 * Return french date
 */
function date_fr($date='',$format='%A %e %B %Y') {
  setlocale(LC_TIME, 'fr_FR.utf8','fra');
  if(empty($date)) { $date = time(); }
  // date_default_timezone_set('Europe/Paris');
  if (isTimestamp($date)) {
    // timestamp
    $maDate = $date;
  } else {
    $maDate = strtotime( str_replace('/', '-', $date) );
  }
  return strftime($format,$maDate);
}

/**
 * Cut string and add "..."
 */
function cut_string($string, $longueur = 120){
  if (empty ($string)){
    return "";
  }
  elseif (strlen ($string) < $longueur){
    return $string;
  }
  elseif (preg_match ("/(.{1,$longueur})\s./ms", $string, $match)){
    return $match [1] . "...";
  }
  else{
    return substr ($string, 0, $longueur) . "...";
  }
}

//----------------- Coupe une chaine en gardant le dernier mot entier
function cutMyWords($chaine,$limit_caracts){
  $last_words = substr($chaine, 0, $limit_caracts+strpos(substr($chaine, $limit_caracts), ' '));
  if (strlen($chaine)!=strlen($last_words)){$last_words.=' [...]';}
  return $last_words;
}

/**
 * Get id by page name
 */
function get_id_by_name($page_name){
  global $wpdb;
  $page_name_id = $wpdb->get_var("SELECT ID FROM $wpdb->posts WHERE post_name = '".$page_name."'");
  return $page_name_id;
}

/**
 * Excerpt limit perso
 * Use : echo excerpt(25);
 */
function excerpt($limit) {
  $excerpt = explode(' ', get_the_excerpt(), $limit);
  if (count($excerpt)>=$limit) {
    array_pop($excerpt);
    $excerpt = implode(" ",$excerpt).'...';
  } else {
    $excerpt = implode(" ",$excerpt);
  }
  $excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
  return $excerpt;
}

/**
 * Content limit perso.
 * Use : echo content(25);
 */
function content($limit) {
  $content = explode(' ', get_the_content(), $limit);
  if (count($content)>=$limit) {
    array_pop($content);
    $content = implode(" ",$content).'...';
  } else {
    $content = implode(" ",$content);
  }
  $content = preg_replace('/\[.+\]/','', $content);
  $content = apply_filters('the_content', $content);
  $content = str_replace(']]>', ']]&gt;', $content);
  return $content;
}

/**
 * Add balise html <pre> for print_r
 */
function pr($cible, $die=false){
  if (is_string($cible)) {
    if (is_json($cible)) {
      $obj = json_decode($cible);
    } else {
      $obj = $cible;
    }
  } else {
    $obj = $cible;
  }
  echo '<pre>';
  print_r($obj);
  echo '</pre>';
  if($die==true){die;}
}

function is_json($string,$return_data=false) {
  $data = json_decode($string);
  return (json_last_error() == JSON_ERROR_NONE) ? ($return_data ? $data : TRUE) : FALSE;
}

//----------------- Permet d'obtenir l'adresse relative à partir d'une URL absolue pour les éléments du site
function rw_remove_root ($url) {
  $url = str_replace( baseurl(get_home_url()), '', $url );
  return '/' . ltrim( $url, '/' );
}


function baseurl($url) {
  $result = parse_url($url);
  return $result['scheme']."://".$result['host'];
}

function basepath() {
  return str_replace($_SERVER["SCRIPT_NAME"],'',$_SERVER["SCRIPT_FILENAME"]);
}









function get_nav($menu_name = "menu_header", $id_menu="", $nav=true){
  if ( ( $locations = get_nav_menu_locations() ) && isset( $locations[ $menu_name ] ) ) {
    $menu = wp_get_nav_menu_object( $locations[ $menu_name ] );
    $menu_items = wp_get_nav_menu_items($menu->term_id);
    $menu_list = '';
    if ($nav) $menu_list = '<nav '.$id_menu.'>';
    $current_url = PROTO.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    foreach ( (array) $menu_items as $key => $menu_item ) {
      $title = $menu_item->title;
      $url = $menu_item->url;
      $class = implode(" ", $menu_item->classes);
      $class .= ' '.alias($title);
      if($url == $current_url){
        $class .= ' current';
      }

      $icon = get_field('pictogramme', $menu_item);
      $svg = '';
      if (!empty($icon)) {
        $svg = svg_icon('icon-'.$icon);
        $class .= ' m--svgicon';
      }

      $menu_list .= '<a ';
      if($class){
        $menu_list .= 'class="'.$class.'"';
      }
      $menu_list .= ' href="' . $url . '" target="'.$menu_item->target.'">'.$svg.'<span>' . $title . '</span></a>';

    }
    if ($nav) $menu_list .= '</nav>';
    return $menu_list;
  }
}

function get_nav_ul($menu_name="menu_header_fr", $id_menu=null, $urls=null){
  $posttype = get_post_type();
  // endwhile; endif;
  if ( ( $locations = get_nav_menu_locations() ) && isset( $locations[ $menu_name ] ) ) {
    $menu = wp_get_nav_menu_object( $locations[ $menu_name ] );
    $menu_items = wp_get_nav_menu_items($menu->term_id);
    if(!empty($id_menu)){$id_menu = ' id="'.$id_menu.'"';}
    $menu_list = '<nav'.$id_menu.'>';
    $menu_list .= build_nav($parent_id=0,$menu_items,$urls);
    $menu_list .= '</nav>';
    return $menu_list;
  }
}


// Fonction récursive pour chaque sous menu
function build_nav($parent_id=0,$menu_items,$urls=null){
  $posttype = get_post_type();
  $posttypeobj = get_post_type_object($posttype);
  $vide = true;
  $html = null;
  $current_urls = array(
    PROTO.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'],
    $_SERVER['REQUEST_URI']
  );
  $numitem = 0;
  foreach ($menu_items as $menu_item) {

    if($menu_item->menu_item_parent==$parent_id){
      $vide = false;

      $title = $menu_item->title;
      $url = $menu_item->url;
      if (!empty($urls)) {
        foreach ($urls as $cle => $valeur) {
          if ($cle==$menu_item->ID) {
            $url = $valeur;
          }
        }
      }

      $class = $menu_item->classes[0];
      if(in_array($url,$current_urls)){
        $class .= ' current';
      }
      if(!empty($posttypeobj->rewrite['slug'])) {
        if(alias($title)==alias($posttypeobj->rewrite['slug'])){
          $class .= ' current';
        }
      }

      $icon = get_field('pictogramme', $menu_item);
      $svg = '';
      if (!empty($icon)) {
        $svg = svg_icon('icon-'.$icon);
        $class .= ' m--svgicon';
      }
      
      $html .= '<li class="item'.$numitem.' '.alias($title).' id'.$menu_item->ID.'">';
      $html .= '<a';
      if($class){
        $html .= ' class="'.$class.'"';
      }
      if(!empty($menu_item->target)) {
        $html .= ' target="'.$menu_item->target.'"';
      }
      $html .= ' href="' . $url . '"><span>' . $title ;
      $desc = trim($menu_item->post_content);
      if(!empty($desc)) {
         $html .= '<br><span class="desc">'.$menu_item->post_content.'</span>';
      }
      $html .= '</span>'.$svg.'</a>';

      $html .= build_nav($menu_item->ID,$menu_items);
      $html .= '</li>';
      $numitem++;
    }
  }
  if(!$vide) {
    return '<ul>'.$html.'</ul>';
  }
}





function alias($chaine){
  $chaine = html_entity_decode($chaine);
  $chaine = preg_replace('/[^a-z0-9]+/i', '-', strtolower(strtoascii($chaine)));
  $chaine = preg_replace ("/-[-]+/", "-", $chaine);
  $chaine = trim($chaine, "-");
  return $chaine;
}

function strtoascii($str, $encoding = 'utf-8') {
  mb_regex_encoding($encoding);
  $str_ascii = array(
    'a'   => 'ÀÁÂÃÄÅĀĂǍẠẢẤẦẨẪẬẮẰẲẴẶǺĄ',
    'a'   => 'àáâãäåāăǎạảấầẩẫậắằẳẵặǻą',
    'c'   => 'ÇĆĈĊČ',
    'c'   => 'çćĉċč',
    'd'   => 'ÐĎĐ',
    'd'   => 'ďđ',
    'e'   => 'ÈÉÊËĒĔĖĘĚẸẺẼẾỀỂỄỆ',
    'e'   => 'èéêëēĕėęěẹẻẽếềểễệ',
    'g'   => 'ĜĞĠĢ',
    'g'   => 'ĝğġģ',
    'h'   => 'ĤĦ',
    'h'   => 'ĥħ',
    'i'   => 'ÌÍÎÏĨĪĬĮİǏỈỊ',
    'i'   => 'ìíîï',
    'j'   => 'Ĵ',
    'j'   => 'ĵ',
    'k'   => 'Ķ',
    'k'   => 'ķ',
    'l'   => 'ĹĻĽĿŁ',
    'l'   => 'ĺļľŀł',
    'n'   => 'ÑŃŅŇ',
    'n'   => 'ñńņňŉ',
    'o'   => 'ÒÓÔÕÖØŌŎŐƠǑǾỌỎỐỒỔỖỘỚỜỞỠỢ',
    'o'   => 'òóôõöøōŏőơǒǿọỏốồổỗộớờởỡợð',
    'r'   => 'ŔŖŘ',
    'r'   => 'ŕŗř',
    's'   => 'ŚŜŞŠ',
    's'   => 'śŝşš',
    't'   => 'ŢŤŦ',
    't'   => 'ţťŧ',
    'u'   => 'ÙÚÛÜŨŪŬŮŰŲƯǓǕǗǙǛỤỦỨỪỬỮỰ',
    'u'   => 'ῡùúûüũūŭůűųưǔǖǘǚǜụủứừửữự',
    'W'   => 'ŴẀẂẄ',
    'w'   => 'ŵẁẃẅ',
    'y'   => 'ÝŶŸỲỸỶỴ',
    'y'   => 'ýÿŷỹỵỷỳ',
    'z'   => 'ŹŻŽ',
    'z'   => 'źżž',
    // Ligatures
    'ae'  => 'Æ',
    'ae'  => 'æ',
    'oe'  => 'Œ',
    'oe'  => 'œ',
    // Exclusions
    '-'   => '_ '
  );

  // Convertion
  foreach ($str_ascii as $k => $v) {
    $str = mb_ereg_replace('['.$v.']', $k, $str);
  }

  return $str;
}





// Read more: http://jaspreetchahal.org/how-to-lighten-or-darken-hex-or-rgb-color-in-php-and-javascript/#ixzz3aaxmuTXi


/*
function lighten($color_code,$percentage = 0) {
  $percentage = str_replace('%','',$percentage);
  return adjustColorLightenDarken($color_code,(-1*$percentage));
}
function darken($color_code,$percentage = 0) {
  $percentage = str_replace('%','',$percentage);
  return adjustColorLightenDarken($color_code,$percentage);
}



function adjustColorLightenDarken($color_code,$percentage_adjuster = 0) {
  $percentage_adjuster = round($percentage_adjuster/100,2);
  // echo $percentage_adjuster.'  ';
  if(is_array($color_code)) {
      $r = $color_code["r"] - (round($color_code["r"])*$percentage_adjuster);
      $g = $color_code["g"] - (round($color_code["g"])*$percentage_adjuster);
      $b = $color_code["b"] - (round($color_code["b"])*$percentage_adjuster);

      return array("r"=> round(max(0,min(255,$r))),
          "g"=> round(max(0,min(255,$g))),
          "b"=> round(max(0,min(255,$b))));
  } else {
      $hex = str_replace("#","",$color_code);
      $r = (strlen($hex) == 3)? hexdec(substr($hex,0,1).substr($hex,0,1)):hexdec(substr($hex,0,2));
      $g = (strlen($hex) == 3)? hexdec(substr($hex,1,1).substr($hex,1,1)):hexdec(substr($hex,2,2));
      $b = (strlen($hex) == 3)? hexdec(substr($hex,2,1).substr($hex,2,1)):hexdec(substr($hex,4,2));
      $r = round($r - ($r*$percentage_adjuster));
      $g = round($g - ($g*$percentage_adjuster));
      $b = round($b - ($b*$percentage_adjuster));

      // echo str_pad(dechex( max(0,min(255,$r)) ),2,"0",STR_PAD_LEFT).str_pad(dechex( max(0,min(255,$g)) ),2,"0",STR_PAD_LEFT).str_pad(dechex( max(0,min(255,$b)) ),2,"0",STR_PAD_LEFT).' ';
      return "#".str_pad(dechex( max(0,min(255,$r)) ),2,"0",STR_PAD_LEFT)
          .str_pad(dechex( max(0,min(255,$g)) ),2,"0",STR_PAD_LEFT)
          .str_pad(dechex( max(0,min(255,$b)) ),2,"0",STR_PAD_LEFT);

  }
}
*/


/*
function get_post_meta_ordered($id,$meta_key){
  global $wpdb;
  $output = array();
  $sql = "SELECT m.meta_value FROM ".$wpdb->postmeta." m where m.meta_key = '".$meta_key."' and m.post_id = '".$id."' order by m.meta_id";
  $results = $wpdb->get_results( $sql );
  foreach( $results as $result ){
    $output[] = $result->meta_value;
  }
  return array_filter($output);
}
*/



// Get SVG Icon
function svg_icon($id, $extraClass=''){
  $tmp = '';
  // $url = revved_asset('img/'.$scope.'.sprite.svg').'#'.$id;
  $url = get_home_url().'/assets/img/sprite.svg#'.$id;
  $tmp .= '<svg role="img" class="svg-icon '.$id.' '.$extraClass.'">';
  $tmp .= '<use xlink:href="'.$url.'"></use>';
  $tmp .= '</svg>';

  return $tmp;
}

/*
// Return specific SVG Icon in a sprite
if (!function_exists('svgIcon')) {
  function svgIcon($id, $extraClass = '', $scope = 'misc'){
    $tmp = '';
    $url = revved_asset('img/'.$scope.'.sprite.svg').'#'.$id;

    $tmp .= '<svg role="img" class="svg-icon '.$id.' '.$extraClass.'">';
    $tmp .= '<use xlink:href="'.$url.'"></use>';
    $tmp .= '</svg>';

    return $tmp;
  }
}
*/

function assets() {
  return get_home_url().'/assets/';
}






/*
$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
$posts_per_page = 3;
$offset = ($posts_per_page * $paged) - $posts_per_page;
$args = array(
  'post_type'       => 'post',
  'post_status'     => 'publish',
  'posts_per_page'  => $posts_per_page,
  'offset'          => $offset
);
$actualites = get_posts($args);

// Pour une page
$postcount = wp_count_posts('post');
$nbr_posts = $postcount->publish;
// Pour une archive
$nbr_pages = $wp_query->max_num_pages;
ou
$term = get_queried_object();
$nbr_posts = $term->count

$num_pages = ceil($nbr_posts / $posts_per_page);


// Pour une page (template) ajouter :
function my_pagination_rewrite() {
  add_rewrite_rule('actualites/page/?([0-9]{1,})/?$', 'index.php?page_id=484&paged=$matches[1]', 'top');
}
add_action('init', 'my_pagination_rewrite');

echo btw_pagination($paged,$num_pages,1,1,get_post_type_archive_link('actualites'));
// OU
echo btw_pagination($paged,$num_pages,1,1,get_permalink());
*/
function btw_pagination($page=1,$maxpage=1,$prevnextbt=false,$minmaxbt=false,$baseurl='',$lang=[]) {

  $lang = array_replace([
    'first' => __('First','btw_pagination_textdomain'),
    'last' => __('Last','btw_pagination_textdomain'),
    'previous' => __('Previous','btw_pagination_textdomain'),
    'next' => __('Next','btw_pagination_textdomain'),
  ], $lang);

  // $lang_default = array(
  //   'first' => __('First','btw_pagination_textdomain'),
  //   'last' => __('Last','btw_pagination_textdomain'),
  //   'previous' => __('Previous','btw_pagination_textdomain'),
  //   'next' => __('Next','btw_pagination_textdomain'),
  // );

  // array_replace($lang_default, $lang);

  if (empty($baseurl)) { $baseurl = get_home_url(); }
  $html = '';
  if ($minmaxbt && $maxpage>1 && $page>1) {
    $firstpage = 1;
    $html .= '<a class="pagenav pagenav_first" href="'.$baseurl.'"><span>'.$lang['first'].'</span></a>';
  }
  if ($prevnextbt && $page>1) {
    $prevpage = $page - 1;
    if ($prevpage==1) {
      $url = $baseurl;
    } else {
      $url = $baseurl.'page/'.$prevpage;
    }
    $html .= '<a class="pagenav pagenav_previous" href="'.$url.'"><span>'.$lang['previous'].'</span></a>';
  }

  if ($page>2) {
    $firstpage = 1;
    $html .= '<a class="pagenav pagenav_firstnum" href="'.$baseurl.'"><span>'.$firstpage.'</span></a>';
    if ($page>3) {
      $html .= '<span class="pagenav pagenav_etc"><span>...</span></span>';
    }
  }

  if ($page>1) {
    $prevpage = $page - 1;
    if ($prevpage==1) {
      $url = $baseurl;
    } else {
      $url = $baseurl.'page/'.$prevpage;
    }
    $html .= '<a class="pagenav" href="'.$url.'"><span>'.$prevpage.'</span></a>';
  }
  if ($maxpage>1) {
    $html .= '<span class="pagenav current" href="'.$baseurl.'page/'.$page.'"><span>'.$page.'</span></span>';
  }
  if ($page<$maxpage) {
    $nextpage = $page + 1;
    $html .= '<a class="pagenav" href="'.$baseurl.'page/'.$nextpage.'"><span>'.$nextpage.'</span></a>';
  }

  if (($maxpage-$page)>=2) {
    $lastpage = $maxpage;
    if (($maxpage-$page)>=3) {
      $html .= '<span class="pagenav pagenav_etc"><span>...</span></span>';
    }
    $html .= '<a class="pagenav pagenav_lastnum" href="'.$baseurl.'page/'.$lastpage.'"><span>'.$lastpage.'</span></a>';
  }

  if ($prevnextbt && $page<$maxpage) {
    $nextpage = $page + 1;
    $html .= '<a class="pagenav pagenav_next" href="'.$baseurl.'page/'.$nextpage.'"><span>'.$lang['next'].'</span></a>';
  }
  if ($minmaxbt && $maxpage>1 && $page<$maxpage) {
    $lastpage = $maxpage;
    $html .= '<a class="pagenav pagenav_last" href="'.$baseurl.'page/'.$lastpage.'"><span>'.$lang['last'].'</span></a>';
  }
  return $html;
}



/*
  0 -> 0.5 = foncé
  0.5 -> 1 = clair
*/
function getLumino($hex) {
  $rgb = ColorUtils::hex2rgb($hex);
  $r = sqrt(pow($rgb[0], 2) * 0.241 + pow($rgb[1], 2) * 0.691 + pow($rgb[2], 2) * 0.068);
  return ($r / 255);
}



function toPhone($tel) {
  $pattern = '/(^0)/i';
  $replacement = '+33';
  $tel2 = preg_replace('/\s+/', '', trim($tel));
  $tel3 = preg_replace($pattern, $replacement, $tel2);  
  return $tel3;
}



function get_page_url_by_template_name($template_name) {

  $pages = get_posts([
    'post_type' => 'page',
    'post_status' => 'publish',
    'meta_query' => [[
      'key' => '_wp_page_template',
      'value' => $template_name.'.php',
      'compare' => '='
    ]]
  ]);

  if(!empty($pages)) {
    foreach($pages as $pages__value) {
      return get_permalink($pages__value->ID);
    }
  }

  return get_bloginfo('url');
}