<?php
function register_cpt_client() {

    $labels = array(
        'name' => _x( 'Clients', 'client' ),
        'singular_name' => _x( 'Client', 'client' ),
        'add_new' => _x( 'Ajouter', 'client' ),
        'add_new_item' => _x( 'Ajouter un client', 'client' ),
        'edit_item' => _x( 'Editer le client', 'client' ),
        'new_item' => _x( 'Nouveau client', 'client' ),
        'view_item' => _x( 'Voirle client', 'client' ),
        'search_items' => _x( 'Rechercher un client', 'client' ),
        'not_found' => _x( 'Aucun client trouvée', 'client' ),
        'not_found_in_trash' => _x( 'Aucun client trouvée dans la corbeille', 'client' ),
        'parent_item_colon' => _x( 'Parent client:', 'client' ),
        'menu_name' => _x( 'Clients', 'client' ),
    );

    $args = array(
		'labels' => $labels,
		'hierarchical' => false,
		'supports' => array( 'title' ),
		//'taxonomies' => array( 'category', 'post_tag' ),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'menu_icon' => 'dashicons-money',
		// Voir la liste => http://melchoyce.github.io/dashicons/
		'show_in_nav_menus' => true,
		'publicly_queryable' => true,
		'exclude_from_search' => false,
		'has_archive' => true,
		'query_var' => true,
		'can_export' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'rewrite' => array( 'slug' => ('clients') ),
    );

    register_post_type( 'client', $args );
}
add_action( 'init', 'register_cpt_client' );
?>
