<?php 
function register_cpt_centre() {

    $labels = array( 
        'name' => _x( 'Centres', 'centre' ),
        'singular_name' => _x( 'centre', 'centre' ),
        'add_new' => _x( 'Ajouter', 'centre' ),
        'add_new_item' => _x( 'Ajouter un centre', 'centre' ),
        'edit_item' => _x( 'Editer la centre', 'centre' ),
        'new_item' => _x( 'Nouvelle centre', 'centre' ),
        'view_item' => _x( 'Voir la centre', 'centre' ),
        'search_items' => _x( 'Rechercher un centre', 'centre' ),
        'not_found' => _x( 'Aucune centre trouvée', 'centre' ),
        'not_found_in_trash' => _x( 'Aucune centre trouvée dans la corbeille', 'centre' ),
        'parent_item_colon' => _x( 'Parent centre:', 'centre' ),
        'menu_name' => _x( 'Centres', 'centre' ),
    );

    $args = array( 
		'labels' => $labels,
		'hierarchical' => false,
		'supports' => array( 'title', 'editor', 'thumbnail' ),
		//'taxonomies' => array( 'category', 'post_tag' ),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		//'menu_icon' => 'dashicons-format-video',
		// Voir la liste => http://melchoyce.github.io/dashicons/
		'show_in_nav_menus' => true,
		'publicly_queryable' => true,
		'exclude_from_search' => false,
		'has_archive' => true,
		'query_var' => true,
		'can_export' => true,
		'rewrite' => true,
		'capability_type' => 'post',
		'rewrite' => array( 'slug' => ('') ),
    );

    register_post_type( 'centre', $args );
}
add_action( 'init', 'register_cpt_centre' );
?>