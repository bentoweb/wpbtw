<?php
/**
 * Disable theme modification for user exept ID 1 !
 */
add_action( 'admin_init', 'dont_change_theme' );
function dont_change_theme() {
  global $submenu;
  unset( $submenu['themes.php'][5] );
  unset( $submenu['themes.php'][15] );
}


/**
 * add button "aller sur le site"
 */
add_action( 'admin_menu' , 'add_admin_menu' );
function add_admin_menu() {
  add_menu_page( 'redirecting', __('Aller sur le site'), 'read', 'home', 'redirect_home', 'dashicons-admin-home', 0);
  add_menu_page( 'deconnexion', __('Déconnexion'), 'read', 'disconnect', 'go_disconnect', 'dashicons-no', 1);
}
function redirect_home() {
  wp_redirect( home_url() );
  exit;
}
function go_disconnect() {
  wp_logout();
  $settings = get_option( 'theme_option' );
  wp_redirect( home_url().'/'.$settings['login_page'] );
  exit;
}


/**
 * Custom style for editor tinymce
 */
add_filter( 'tiny_mce_before_init', 'custom_tinymce' );
if ( !function_exists('custom_tinymce')) {
  function custom_tinymce( $tools ) {
    // $tools['theme_advanced_buttons2'] = 'styleselect,'.$tools['theme_advanced_buttons2'];
    // $tools['theme_advanced_styles'] = __('Text vert').'=txt-vert';
    $tools['block_formats'] = 'Paragraphe=p;Titre 2=h2;Titre 3=h3;Titre 4=h4;Citation=blockquote';
    return $tools;
  }
}


/**
 * Rajouter bouton sur editeur hr etc...
 */
function enable_more_buttons($buttons) {
  // $buttons[] = 'hr';
  // $buttons[] = 'fontselect';
  // $buttons[] = 'sup';
  // return $buttons;
}
// add_filter("mce_buttons", "enable_more_buttons");


/**
 * Add custom css for wp admin
 */
function back_office_css() {
  wp_enqueue_style( 'admin_css', get_template_directory_uri().'/assets/css/backoffice.css' );
}
add_action('admin_head', 'back_office_css', 11 );
/**
 * Add custom css for editor tinymce
 */
// add_action( 'after_setup_theme', 'init_editor_styles' );
// if ( !function_exists('init_editor_styles')) {
//   function init_editor_styles() {
//     add_editor_style('/assets/css/editor.css');
//   }
// }

/**
 * Add custom css for editor tinymce
 */
/**
 * Registers an editor stylesheet for the theme.
 */
function wpdocs_theme_add_editor_styles() {
  add_editor_style( get_stylesheet_directory_uri().'/assets/editor.css' );
}
add_action( 'admin_init', 'wpdocs_theme_add_editor_styles' );




function remove_dashboard_widgets() {
  global $wp_meta_boxes;

  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_drafts']);
  unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
  unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);

}

add_action('wp_dashboard_setup', 'remove_dashboard_widgets' );


/**
 * add a logo on left menu top
 */
function admin_logoadmin() {
  echo '<script type="text/javascript">
    jQuery(document).ready(function(){
      jQuery("#adminmenuwrap").prepend("<img src=\"'.assets().'img/logoadmin.png\" style=\"width:135px;height:auto;margin:10px 0 0 10px;\">");
    });
  </script>';
}
add_action('admin_head', 'admin_logoadmin');




/*
  ADD MENUS TO ADMIN MENU
*/
function custom_admin_menu() {
  global $menu;
  global $submenu;
  unset($submenu['themes.php']);
  // $menu[60] = array( 0 => 'Menus', 1 => 'edit_theme_options', 2 => 'nav-menus.php', 4 => 'menu-top menu-icon-generic', 5 => 'menu-appearance', 6 => 'div' ); // Change le menu "Apparence" en "Menus"
  add_menu_page( 'Menus', 'Menus', 'manage_options', 'nav-menus.php', '', 'dashicons-menu', 10 );
}
add_action('admin_menu', 'custom_admin_menu', 11);






// Add the custom columns to the book post type:
add_filter( 'manage_page_posts_columns', 'set_custom_edit_book_columns' );
function set_custom_edit_book_columns($columns) {
  $columns['pagetemplate'] = __( 'Modèle', 'your_text_domain' );

  return $columns;
}

// Add the data to the custom columns for the book post type:
add_action( 'manage_page_posts_custom_column' , 'custom_page_column', 10, 2 );
function custom_page_column( $column, $post_id ) {
  switch ( $column ) {

    case 'pagetemplate' :
      $template_path = get_post_meta($post_id, '_wp_page_template', true);
      if (!empty($template_path)) {
        $templates = wp_get_theme()->get_page_templates();
        if (!empty($templates[$template_path])) {
          echo $templates[$template_path];
        }
      }
      break;

  }
}