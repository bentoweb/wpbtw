<?php
require_once('utils.php');
require_once('utils/genimg.php');

require_once('theme.php');
require_once('rewrites.php');
require_once('clean.php');
if (is_admin()) {
  require_once('backoffice.php');
}

// Admin elements
require_once('adminutils/cpt-adminutils.php');
require_once('adminutils/admin.php');


require_once('cto/cto-acf-siteoptions.php');


require_once('libs/ColorUtils.php');

