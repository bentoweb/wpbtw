(function($) {

  var btwApp = {

    docReady: false,
    docLoad: false,

    ajax_lock: false,
    resize_lock: false,

    getUrlParameter: function getUrlParameter(sParam) {
      var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;
      for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] === sParam) {
          return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
      }
    },

    //-------- Responsive
    wW: 0,
    wH: 0,
    dH: 0,
    resptimer: false,
    responsiveItems: [],

    responsive: function(f) {
      if (typeof f === "function") {
        btwApp.responsiveItems.push(f);
      }
    },

    goResponsive: function() {
      btwApp.wW = $(window).width();
      btwApp.wH = $(window).height();
      for (var key in btwApp.responsiveItems) {
        btwApp.responsiveItems[key](btwApp.wW,btwApp.wH); // run your function
      }
    },

    onWindowResize: function() {
      btwApp.resize_lock = true;
      clearTimeout(btwApp.resptimer);
      btwApp.resptimer = setTimeout(function(){
        btwApp.resize_lock = false;
        btwApp.goResponsive();
      }, 500);
    },

    readyAndLoadedItems: [],

    readyAndLoaded: function(f) {
      if (typeof f === "function") {
        btwApp.readyAndLoadedItems.push(f);
      }
    },

    readyAndLoadedMain: function() {
      if (!btwApp.docLoad || !btwApp.docReady) { return; }
      btwApp.onWindowResize();
      for (var key in btwApp.readyAndLoadedItems) {
        btwApp.readyAndLoadedItems[key](); // run your function
      }
    }

  };
  window.btwApp = btwApp;





  // function readyAndLoaded() {
    // if (!btwApp.docLoad || !btwApp.docReady) { return; }
    // Scripts here
    // btwApp.onWindowResize();
  // }

  $(window).on("load", function() {
    btwApp.docLoad = true;
    btwApp.readyAndLoadedMain();
  });

  $(document).ready(function(){
    btwApp.docReady = true;
    btwApp.readyAndLoadedMain();
  });




  if (window.addEventListener) {
    window.addEventListener("resize", function(){ btwApp.onWindowResize(); }, false);
    window.addEventListener('orientationchange', function(){ btwApp.onWindowResize(); }, false);
  } else {
    window.attachEvent("onresize", function(){
      btwApp.onWindowResize(); });
  }




})(jQuery);
