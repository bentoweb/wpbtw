<?php

// GANDI
if (WP_DEBUG) {
	header("Cache-Control: max-age=1");
	error_reporting(E_ALL | E_WARNING | E_NOTICE);
	ini_set("display_errors", 1);
	ini_set('session.gc_maxlifetime', 3600 * 2 );
}



$modemaintenance = get_field('activer_le_mode_maintenance',7);
if (!empty($modemaintenance)) {
	global $wp_query;
  set_query_var( 'btw-maintenance-mode', true );
	if (!is_user_logged_in() && !in_array($wp_query->post->ID, array(5,6,7))) {
		wp_safe_redirect(get_permalink(7),302);
	}
} else {
  set_query_var( 'btw-maintenance-mode', false );
}



?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
	<meta http-equiv="content-language" content="<?php echo get_bloginfo('language'); ?>" />
	<meta name='viewport' content="width=device-width, initial-scale=1.0, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes" />

	<link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_home_url() ?>/fav/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_home_url() ?>/fav/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_home_url() ?>/fav/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_home_url() ?>/fav/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_home_url() ?>/fav/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_home_url() ?>/fav/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_home_url() ?>/fav/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_home_url() ?>/fav/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_home_url() ?>/fav/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192" href="<?php echo get_home_url() ?>/fav/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_home_url() ?>/fav/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php echo get_home_url() ?>/fav/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_home_url() ?>/fav/favicon-16x16.png">
	<link rel="manifest" href="<?php echo get_home_url() ?>/fav/manifest.json" crossorigin="use-credentials">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="<?php echo get_home_url() ?>/fav/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">

	<!-- <link rel="icon" type="image/png" href="<?php bloginfo('template_directory'); ?>/assets/img/fav.png" /> -->
	<title><?php /*if( is_404() ) echo '404 '; else $t = get_the_title(); echo striptags_br($t);*/if ( is_search() ) { echo 'Recherche : "'.get_search_query().'"'; } else { wp_title(); } ?></title>
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css">
	<!--[if lt IE 9]> <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script> <![endif]-->
	<?php wp_head(); ?>

	<?php
	// global $theme_option; $theme_settings = get_option( 'theme_option', $theme_option );
	?>
	<script>
	var home_url = '<?php echo home_url() ?>';
	</script>

</head>

<?php
if ( is_front_page() ) { $classe = 'homepage'; }
else if (is_archive()) { $classe = 'archive'; }
else if (is_page()) { $classe = 'page'; }
else if (is_search()) { $classe = 'search'; }
else if (is_single()) { $classe = 'single'; }
else { $classe = ''; }

if (!empty($post)) {
	$template_path = get_post_meta($post->ID, '_wp_page_template', true);
	// pr($template_path);
	if (!empty($template_path)) {
	  // $templates = wp_get_theme()->get_page_templates();
	  // if (!empty($templates[$template_path])) {
	    $classe .= ' tpl-'.$template_path;
	  // }
	}
}
?>

<body class="<?php echo $classe ?>">


	<?php
	require_once(dirname(__FILE__).'/../'.WP_DEFAULT_THEME.'/htmlheader.php');
	// pr(ENV);
	?>
