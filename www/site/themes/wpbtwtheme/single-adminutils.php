<?php
// pr(get_stylesheet_directory());
// pr(get_stylesheet_directory_uri());
// pr(get_stylesheet_uri());

// $childpath = get_stylesheet_directory();
// exit;

if (have_posts()) : while (have_posts()) : the_post();

  // if ($post->post_title=='Admin') {
  if ($post->ID==5) {
    get_template_part('setup/adminutils/login');
  }
  // else if ($post->post_title=='Reset') {
  else if ($post->ID==6) {
    get_template_part('setup/adminutils/reset-password');
  }
  // else if ($post->post_title=='Maintenance') {
  else if ($post->ID==7) {
    get_template_part('setup/adminutils/maintenance');
  }
  // else if ($post->post_title=='404') {
  else if ($post->ID==13) {
    get_template_part('setup/adminutils/404');
  }
  else {
    if (file_exists($childpath.'/404.php')) {
      get_template_part($childpath.'/404.php');
    } else {
      wp_safe_redirect( get_bloginfo('url').'/404' );
    }
  }

endwhile; endif;

