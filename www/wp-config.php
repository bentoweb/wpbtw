<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clés secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C’est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

/*
WAMP : httpd-vhosts.conf, add line "SetEnv env dev" for a vhost
*/
// $env = getenv('env');
// if (!empty($env)) {
//   define( 'ENV', $env);
// } else {
//   define( 'ENV', '');
// }

// WP_DEBUG à true en dev, et false en prod
define( 'WP_DEBUG', true);

if (WP_DEBUG) {
  header("Cache-Control: max-age=1");
  error_reporting(E_ALL | E_WARNING | E_NOTICE);
  ini_set("display_errors", 1);
  ini_set('session.gc_maxlifetime', 3600 * 2 );
}


$url = 'http://wpbtw.local/';
define('WP_DEFAULT_THEME', 'wpbtwtheme-child');

define( 'WP_SITEURL', $url);
define( 'WP_HOME', $url);

define( 'DB_NAME', 'wpbtw' );
define( 'DB_USER', 'wpbtw' );
define( 'DB_PASSWORD', 'in64kAraMSBVm8qf' );
define( 'DB_HOST', 'localhost' );
define( 'DB_CHARSET', 'utf8mb4' );
define( 'DB_COLLATE', '' );
$table_prefix = 'wpbtw_';// Variable apparemment indispensable pour WP
define( 'TABLE_PREFIX', $table_prefix );
define( 'WPLANG', 'fr_FR' );
// echo TABLE_PREFIX;
// exit;

// $protocol = stripos($_SERVER['SERVER_PROTOCOL'],'https') === 0 ? 'https://' : 'http://';
if ((!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443) {
  $protocol = 'https://';
} else {
  $protocol = 'http://';
}
define( 'PROTO', $protocol );

// define('WP_MEMORY_LIMIT', '300M');
// define( 'TINIFYKEY', 'tinify site key'); // Commenter si inexistant

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'M<snPN>1yknIcS6cG`t_/|]U<S%T^XF(}%s}JxQrm5jICZbMzdgz}Ax^ThBzc3gs' );
define( 'SECURE_AUTH_KEY',  'azM#G4Izc$(!+CKy#$8:2KfV<KO71n1CP{5~s{,+j(;`WjL(0|?AX3>Hv#?i @0-' );
define( 'LOGGED_IN_KEY',    '=sYtj?jfod`!I6WmxU~/((&gzwQ:b20cfEkaoaOQ{Py)atJzsnNS.AqcDWGX|YP*' );
define( 'NONCE_KEY',        'F!pJKw[]mU9cTyi#EmiBG3J&dl&<LSJZV-K;mU3TDr4pWO41u}CfJ}GEAR]MKiVm' );
define( 'AUTH_SALT',        'iU2gmh$df}4/AXt{o!_7_I+T5f8.A/[&SY5wJ|VmXDm:8GKm-YjJSJf5C.o?K(cB' );
define( 'SECURE_AUTH_SALT', '26rC=&l/Ktl=0K%+U>WfP}LsKz@0n4w-4MaUF.IIE&sZqTrY,-RLPC[f*I,aWy5~' );
define( 'LOGGED_IN_SALT',   ' 7}MHINrec]DZK89)j,Q2|[Y+%s1ldMZ]C2!@YT 7#UOh:b^b?x>lk(O{uVrZSp!' );
define( 'NONCE_SALT',       'h@ofbX;c>j=J?`B(kYk|z~%(4[,MQ>)S^ZV1}1oSk>R;f*-vQ9P)c{,ZBFhYCiYn' );
/**#@-*/



/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */

/* C’est tout, ne touchez pas à ce qui suit ! Bonne publication. */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');


define('UPLOADS', 'medias');

// $perfect_url = trim(WP_SITEURL,'/');

// define ('WP_CONTENT_FOLDERNAME', '/content');
define ('WP_CONTENT_FOLDERNAME', 'site');
define ('PERFECT_URL', WP_SITEURL);
define ('PERFECT_URL_COMPLETE', WP_SITEURL);
define ('WP_CONTENT_URL', WP_SITEURL.WP_CONTENT_FOLDERNAME);
define ('WP_CONTENT_DIR', ABSPATH.WP_CONTENT_FOLDERNAME);
define ('WP_PLUGIN_DIR', WP_CONTENT_DIR . '/plugins');
define ('WP_PLUGIN_URL', WP_CONTENT_URL.'/plugins');

define('AUTOSAVE_INTERVAL', 300 ); // seconds (300=5min)
define('WP_POST_REVISIONS', false );

if ( !defined('MYABSPATH') ) {
  $absp = str_replace('\\', '/', dirname(__FILE__)).'/';
  define('MYABSPATH', $absp);
}


/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');
