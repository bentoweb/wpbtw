/*
	v.2.0.1
*/
var themename = 'wpbtwtheme-child';
var parentthemename = 'wpbtwtheme';

var gulp 	= require('gulp'),
	sass 	= require('gulp-sass')(require('node-sass')),
	concat 	= require('gulp-concat'),
	cleancss = require('gulp-clean-css'),
	uglify 	= require('gulp-uglify'),
	rename 	= require('gulp-rename'),
	// livereload = require('gulp-livereload');
	sourcemaps = require('gulp-sourcemaps');

var parentpath = 'www/site/themes/'+parentthemename+'/assets/';
var childpath = 'www/site/themes/'+themename+'/assets/';

/*
---------------------------- CSS
*/

function stylesdev() {
	var d = new Date();
	var h = d.getHours();
	var m = d.getMinutes();
	console.log('------ styles '+h+':'+m);
	return gulp
	.src(childpath+'css/styles.scss', {
	  sourcemaps: true
	})
	.pipe(sass())
	.pipe(cleancss())
	.pipe(rename({
	  basename: 'output-dev',
	  //suffix: '.min'
	}))
	.pipe(sourcemaps.write())
	.pipe(gulp.dest(childpath))
	// .pipe(livereload());
}

function stylesprod() {
	return gulp
	.src(childpath+'css/styles.scss', {
	  sourcemaps: false
	})
	.pipe(sass())
	.pipe(cleancss())
	.pipe(rename({
	  basename: 'output',
	  //suffix: '.min'
	}))
	.pipe(gulp.dest(childpath));
}

function wpeditorstyles() {
	return gulp
	.src(childpath+'css/editor.scss', {
	  sourcemaps: true
	})
	.pipe(sass())
	.pipe(cleancss())
	.pipe(rename({
	  basename: 'editor',
	  //suffix: '.min'
	}))
	.pipe(gulp.dest(childpath));
}

/*
---------------------------- JS
*/

const scriptsfiles = [
  parentpath+'js/scripts.js',
  // parentpath+'js/*.js',
  // childpath+'js/base.js',
  // childpath+'js/slick.min.js',
  childpath+'js/lib/leaflet.js',
  childpath+'js/scripts.js',
  childpath+'js/b_menu.js',
  childpath+'js/b_faq.js',
  childpath+'js/b_map.js',
];

function scriptsdev() {
	var d = new Date();
	var h = d.getHours();
	var m = d.getMinutes();
	console.log('------ scripts '+h+':'+m);
	return gulp
	// .src([parentpath+'js/*.js',childpath+'js/*.js'], {
	.src(scriptsfiles, {
		sourcemaps: true
	})
	// .pipe(uglify())
	.pipe(concat('output-dev.js'))
	// .pipe(rename({
	  // basename: 'output-dev',
	// }))
	.pipe(gulp.dest(childpath))
	// .pipe(livereload());
}

function scriptsprod() {
	return gulp
	// .src([parentpath+'js/*.js',childpath+'js/*.js'], {
	.src(scriptsfiles, {
		sourcemaps: false
	})
	.pipe(uglify())
	.pipe(concat('output.js'))
	.pipe(gulp.dest(childpath))
	// .pipe(livereload());
}

/*
---------------------------- WATCH
*/

function watchfiles() {
	// livereload.listen({host : 'localhost'});
	// gulp.watch(childpath+'**/*', { ignoreInitial: true }, livereload.reload);
	gulp.watch(childpath+'**/*', { ignoreInitial: true });
	// gulp.watch(childpath+'templates/**/*', { ignoreInitial: true }, livereload.reload);
	gulp.watch(childpath+'css/**/*', stylesdev);
	gulp.watch(childpath+'css/**/*', stylesprod);
	gulp.watch(childpath+'css/**/*', wpeditorstyles);
	gulp.watch(childpath+'js/*.js', scriptsdev);
	gulp.watch(childpath+'js/*.js', scriptsprod);
	// gulp.watch(parentpath+'css/**/*', styles);
	gulp.watch(parentpath+'js/*.js', scriptsdev);
	gulp.watch(parentpath+'js/*.js', scriptsprod);
}

const watch = gulp.parallel(watchfiles);
const build = gulp.series(gulp.parallel(stylesdev,stylesprod,scriptsdev,scriptsprod,wpeditorstyles),watch);

exports.default = build;
