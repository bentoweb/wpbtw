# WPBTW Theme #
**Versions**
* WordPress 5.9
* Parent 0.2
* Child 0.10
* Node 16.13.2

**LAST UPDATE**
26/01/2022

(Documentation en cours de rédaction)

Ceci est une base de travail pour mettre en place un site sur mesure sous WordPress. Cela consiste à préparer des outils pour un développeur expérimenté afin de gagner du temps tout en garantissant légèreté et performance pour le site final.

En plus de fichers et fonctions préparés, ce projet cadre également les outils et pratiques à observer, notamment en CSS et JS.

En attendant l'installeur automatique, il est nécessaire de modifier manuellement à l'installation les fichiers sql, wpconfig, le nom du dossier du thème enfant et sont style.css.

Rien ne devrait avoir à être modifié dans le thème parent wpbtwtheme. Dans le cas contraire, c'est une correction à intégrer au dépôt de cet outil.

## CSS ##
### Organisation et usage ###
* Les fichiers sont minifiés dans assets/output.css via Gulp. Le nom du thème enfant doit être spécifié dans le gulpfile.js.
* Tous les fichiers css et scss ) à intégrer sont inclus dans styles.scss.
* Les variables sont déclarées dans le fichier init.scss.
* Le fichier base.scss sert à regrouper les réglages généraux.
* Utiliser un fichier commençant par p_ pour les styles spécifiques à une page (un template) en particulier.
* Utiliser un fichier commençant par b_ pour les styles d'un bloc pouvant se retrouver sur plusieurs pages (comme un social wall, une preview d'actualités, le header, le menu, etc.)

## JAVASCRIPT ##
Les fichiers sont minifiés dans assets/output.js via Gulp. Le nom du thème enfant doit être spécifié dans le gulpfile.js.
La liste des fichiers JS à intégrer dans dans le tableau **scriptsfiles** dans gulpfile.js

## PHP ##
Outils et organisation des fichiers

## WAMP ##
**hosts :**
127.0.0.1 wpbtw.local
**https-vhosts.conf :**
<VirtualHost *:80>
  ServerName wpbtw.local
  ServerAlias wpbtw.local
  DocumentRoot "C:\_Projets\wpbtw\www"
  <Directory "C:\_Projets\wpbtw\www">
	AllowOverride All
	Options Indexes FollowSymLinks
	Require local
  </Directory>
</VirtualHost>